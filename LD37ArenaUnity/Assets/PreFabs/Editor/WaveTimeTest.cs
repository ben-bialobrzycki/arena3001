﻿using UnityEngine;
using UnityEditor;
using NUnit.Framework;
using System;

public class WaveTimeTest {

	[Test]
	public void EditorTest() {
		//Arrange
		var gameObject = new GameObject();

        LevelManager levelManager = new LevelManager();
        levelManager.InitLevels();
        for (int i = 1; i < 25; i++)
        {
            levelManager.WaveNumber = i;
            int enemies = levelManager.GetNumberOfEnemiesForWave();
            float secondsToWave = levelManager.GetNumberOfSecondsForNextWave();
            float enemiesPerSec = enemies / secondsToWave;
            Console.WriteLine("Wave {0} time {1} enemies {2} enemies per sec {3}", i, secondsToWave, enemies, enemiesPerSec);
        }
	
		var newGameObjectName = "My game object";
		gameObject.name = newGameObjectName;

		Assert.AreEqual(newGameObjectName, gameObject.name);
	}
}
