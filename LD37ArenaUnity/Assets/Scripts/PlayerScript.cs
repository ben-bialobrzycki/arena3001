﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using Assets.Scripts;

public class PlayerScript : MonoBehaviour
{
    Rigidbody2D body;
    Animator animator;
    SpriteRenderer spriteRenderer;
    public GameObject Bullet;
    public GameObject FallAnimation;
    public float BulletSpeed = 4.0f;
  
    Character characterBehaviour;
    LevelManager levelManager;
    public GameObject MouseCursorPreFab;

    public GameObject MeleeWeaponPreFab;

    public GameObject SwordPreFab;
    protected MouseInputBehaviour mouseBehaviour;

    public AudioClip[] PlayerTakeDamage;
    public AudioClip[] PlayerFire;
    public AudioClip[] PlayerSwordSlash;

    Vector2 velocity;
    public float FallOffsetDistance = 0.5f;
    bool falling;

    public float speed = 2f;

    private float actualSpeed = 2f;
    public float minDead = 0.01f;

    int CurrentAnimationIndex = 0;
    List<Vector2> vecDirs;

    public float FireResetTime = 1.0f;
    float actualFireResetTime = 1.0f;
    public Vector3 FireOffset = new Vector3(0, -0.25f, 0f);

    bool Firing;
    bool _trying_to_fire;

    GameObject myAttack;
    MeleeAttack meleeattackBehaviour;
    public float meleeAttackOffset = 1.0f;
    public PlayerWeapon DefaultWeapon = PlayerWeapon.Sword;
    Vector2 CurrentDirection;

    [HideInInspector]
    public PlayerWeapon CurrentWeapon;
    public bool IsDead()
    {
        if (characterBehaviour == null)
            return false;
        return characterBehaviour.Health <= 0;
    }


    void Start()
    {
        this.CurrentWeapon = DefaultWeapon;
        this.body = GetComponent<Rigidbody2D>();
        this.animator = GetComponent<Animator>();
        spriteRenderer = GetComponent<SpriteRenderer>();
        this.characterBehaviour = GetComponent<Character>();
        this.characterBehaviour.SupportsTemporaryImmunity = true;
        this.characterBehaviour.DestroyOnDie = false;
        this.animator.speed = 0;
        SetInitialDirection();
  
        this.characterBehaviour.PointValue = 0;
        levelManager = FindObjectOfType(typeof(LevelManager)) as LevelManager;
        SetVectorDirectionList();
        ReDisplayHealth();
  
        myAttack = Instantiate(MeleeWeaponPreFab) as GameObject;
        meleeattackBehaviour = myAttack.GetComponent<MeleeAttack>();
        if (levelManager.GameOptions.ControlScheme == InputScheme.Mouse)
        {
            InitialiseMouseInput();
        }
        ResetDefaults();
    }

    private void ResetDefaults()
    {
        this.actualSpeed = speed;
        actualFireResetTime = FireResetTime;
    }

    void InitialiseMouseInput()
    {
        var canvas = levelManager.GetCanvasObject();
        var mouseObject = GameObject.Instantiate(MouseCursorPreFab, canvas.transform);
        this.mouseBehaviour = mouseObject.GetComponent<MouseInputBehaviour>();
        levelManager.MouseCursor = mouseObject;
    }

    void RemoveMouseInput()
    {
        mouseBehaviour = null;
    }

    public void DoReset()
    {
        SetInitialDirection();
        SetAnimationForCurrentDirection(CurrentDirection,true);
        this.Firing = false;
        this.falling = false;
        ResetDefaults();
    }

    public void SetInitialDirection()
    {
        CurrentDirection = Vector2.up;
    }

    internal void AddHealth(float healthValue)
    {
        this.characterBehaviour.AddHealth(healthValue);
        ReDisplayHealth();
    }

    public void SetWeapon(PlayerWeapon weapon)
    {
        this.CurrentWeapon = weapon;
    }
    IEnumerator ResetSpeed(float duration)
    {
        yield return new WaitForSeconds(duration);
        this.actualSpeed = speed;
    }

    IEnumerator ResetFire(float duration)
    {
        yield return new WaitForSeconds(duration);
        this.actualFireResetTime = FireResetTime;
        this.meleeattackBehaviour.SetDuration(this.meleeattackBehaviour.Duration);
    }

    internal void AddSpeed(float speedValue, float duration)
    {
        this.actualSpeed += speedValue;
        StopCoroutine("ResetSpeed");
        StartCoroutine("ResetSpeed", duration);
    }

    internal void AddFireup(float speedValue, float duration)
    {
        this.actualFireResetTime -= speedValue;
        this.meleeattackBehaviour.SetDuration(this.meleeattackBehaviour.Duration - speedValue);
        StopCoroutine("ResetFire");
        StartCoroutine("ResetFire", duration);
    }

    void SetVectorDirectionList()
    {
        vecDirs = new List<Vector2>();
        vecDirs.Add(Vector2.down);
        Vector2 dleft = (Vector2.down + Vector2.left);
        dleft.Normalize();
        vecDirs.Add(dleft);
        vecDirs.Add(Vector2.left);
        Vector2 uleft = Vector2.up + Vector2.left;
        uleft.Normalize();
        vecDirs.Add(uleft);
        vecDirs.Add(Vector2.up);
        Vector2 uright = Vector2.up + Vector2.right;
        uright.Normalize();
        vecDirs.Add(uright);
        vecDirs.Add(Vector2.right);
        Vector2 dright = Vector2.right + Vector2.down;
        dright.Normalize();
        vecDirs.Add(dright);

    }
  

    Vector2 FindClosest8WayDirectionForVector(Vector2 direction)
    {
        float max = 0;
        int selected = 0;
        for (int i = 0; i < vecDirs.Count; i++)
        {
            float dot = Vector2.Dot(vecDirs[i], direction);
            if (dot > max)
            {
                max = dot;
                selected = i;
            }

        }
        return vecDirs[selected];
    }

    void SetAnimationForCurrentDirection(Vector2 direction, bool forceReset)
    {
        Vector2 currDir = FindClosest8WayDirectionForVector(direction);
        int selected  = vecDirs.IndexOf(currDir);

        if (CurrentAnimationIndex != selected || forceReset)
        {
            CurrentAnimationIndex = selected;
            this.animator.SetTrigger("Reset");
            this.animator.SetInteger("Direction", CurrentAnimationIndex);
        }
        
    }


    Vector2 GetVectorForTravelDirection()
    {
        float horizontal = Input.GetAxis("Horizontal");
        float vertical = Input.GetAxis("Vertical");


        if (Mathf.Abs(horizontal) < minDead)
            horizontal = 0.0f;

        if (Mathf.Abs(vertical) < minDead)
            vertical = 0.0f;

        Vector2 vec = new Vector2(horizontal, vertical);
        return vec;
    }

    void GetClassicInput()
    {
        Vector2 vec = GetVectorForTravelDirection(); 
        float mag = vec.SqrMagnitude();
        if (mag < minDead)
        {
            vec = Vector2.zero;
        }
        else
        {
            vec.Normalize();
            vec = FindClosest8WayDirectionForVector(vec);
            CurrentDirection = vec;
            SetAnimationForCurrentDirection(CurrentDirection,false);
        }

        SetVelocityAndAnimation(vec);
    }


    Vector2 GetVectorForRightStick()
    {
        float horizontal = Input.GetAxis("RightStickHorizontal");
        float vertical = Input.GetAxis("RightStickVertical");


        if (Mathf.Abs(horizontal) < minDead)
            horizontal = 0.0f;

        if (Mathf.Abs(vertical) < minDead)
            vertical = 0.0f;

        Vector2 vec = new Vector2(horizontal, vertical);
        return vec;
    }

    void GetTwinStickInput()
    {
        Vector2 vec = GetVectorForTravelDirection(); 
        float mag = vec.SqrMagnitude();
        if (mag < minDead)
        {
            vec = Vector2.zero;
        }
        else
        {
            vec.Normalize();
        }

        Vector2 fireDir = GetVectorForRightStick();
      
        if (fireDir.sqrMagnitude > minDead)
        {
            fireDir.Normalize();
            SetAnimationForCurrentDirection(FindClosest8WayDirectionForVector(fireDir),false);
            CurrentDirection = fireDir;
            _trying_to_fire = true;
            TryFire();
        }
        else
        {
            _trying_to_fire = false;
        }

        SetVelocityAndAnimation(vec);
    }

    void GetMouseInput()
    {
        if (this.mouseBehaviour == null)
            InitialiseMouseInput();

        RaycastHit2D rayHit = this.mouseBehaviour.GetWorldPosition();

        if (rayHit.collider == null)
        {
            Debug.Log("Could not get raycast");
            return;
        }
     
        Vector3 mousePosition = rayHit.point;
        Vector3 targetDir = mousePosition - this.transform.position;
        targetDir.Normalize();
        var vec = FindClosest8WayDirectionForVector(targetDir);
        CurrentDirection = targetDir; // vec;
        SetAnimationForCurrentDirection(vec,false);


        Vector2 vecMove = GetVectorForTravelDirection();
        if (vecMove.sqrMagnitude < minDead)
        {
            this.animator.speed = 0;
            velocity = Vector2.zero;
            return;
        }
        vecMove.Normalize();
     
        SetVelocityAndAnimation(vecMove);

    }

    private void SetVelocityAndAnimation(Vector2 vecMove)
    {
        if (vecMove == Vector2.zero)
            this.animator.speed = 0.0f;
        else
            this.animator.speed = actualSpeed / speed;// 1;

        velocity = vecMove * actualSpeed;
    }

    private void FixedUpdate()
    {
        this.body.MovePosition(this.body.position + (velocity * Time.deltaTime));
    }


    // Update is called once per frame
    void Update()
    {

        if (levelManager.Paused)
            return;
        switch (levelManager.GameOptions.ControlScheme)
        {
            case InputScheme.Mouse:
                GetMouseInput(); break;
            case InputScheme.Twinstick:
                GetTwinStickInput(); break;
            case InputScheme.Classic:
                GetClassicInput();
                break;
        }

        if (CurrentWeapon == PlayerWeapon.Sword)
        {
            if (meleeattackBehaviour != null && meleeattackBehaviour.IsAttacking)
                velocity = Vector2.zero;
        }

        if (IsFireButtonDown())
            _trying_to_fire = true;
        else
            _trying_to_fire = false;
        TryFire();

        if (this.characterBehaviour.Immune)
        {
            this.spriteRenderer.color = Color.magenta;
        }
        else
            this.spriteRenderer.color = Color.white;
    }

    private void TryFire()
    {
        if (!this.Firing && _trying_to_fire)
        {
            StartCoroutine(FireReset());
        }
    }

    public void RunTemporaryImmunity()
    {
        this.characterBehaviour.RunTemporaryImmunity();
    }
    
    IEnumerator FireReset()
    {
        Firing = true;
        DoFire();
        yield return new WaitForSeconds(actualFireResetTime);
        Firing = false;
    }

    
    bool IsFireButtonDown()
    {
        return Input.GetKey(KeyCode.Space) || Input.GetButton("Fire1");
    }

    
    public void TryTakeDamage(GameObject fromObject)
    {
        var dmg = fromObject.GetComponent<InflictDamage>();
        if (dmg != null)
        {
            if (!this.characterBehaviour.Immune)
                levelManager.PlayOneShot(PlayerTakeDamage);
            this.characterBehaviour.TakeDamage(dmg.DamageValue,true);
            ReDisplayHealth();
        }
    }

  
    public void PlayFallAnimation(Vector3 position)
    {
        GameObject scorps = FallAnimation;
        Instantiate(scorps, position, Quaternion.identity);
    }

    
    public void TakeFall(GameObject fromObject)
    {
        if (falling)
            return;
        falling = true;
        var dmg = fromObject.GetComponent<InflictDamage>();
        if (dmg != null)
        {
            this.characterBehaviour.Immune = false;
            this.characterBehaviour.TakeDamage(dmg.DamageValue,false);
            ReDisplayHealth();

        }
        Vector3 fallObjectDir = (fromObject.transform.position - this.transform.position);
        fallObjectDir.Normalize();

        Vector3 fallPos = this.transform.position + (FallOffsetDistance * fallObjectDir);

        PlayFallAnimation(fallPos);
        if (!IsDead())
            levelManager.DoPlayerReset(this.gameObject);
       
    }

    void ReDisplayHealth()
    {
        this.levelManager.ShowPlayerHealth((int)this.characterBehaviour.Health);
    }


    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.tag == TagUtil.Hazard)
        {
            Debug.LogFormat("Player Triggerd OnCollisionEnter");
            TakeFall(collision.gameObject);
        }
    }
    public void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == TagUtil.Player)
            return;
        if (collision.gameObject.tag == TagUtil.Enemy)
        {
            TryTakeDamage(collision.gameObject);
        }

    }
    void DoFire()
    {
        if (CurrentWeapon == PlayerWeapon.SimpleGun)
            ProjectileFire();
        else if (CurrentWeapon == PlayerWeapon.Sword)
            MeleeAttack();
    }


    private void MeleeAttack()
    {
        if (meleeattackBehaviour != null && meleeattackBehaviour.IsAttacking)
            return;
        levelManager.PlayOneShot(PlayerSwordSlash, false);

        Quaternion rotation = Quaternion.identity;
        
        Vector2 currDir8 = FindClosest8WayDirectionForVector(CurrentDirection);
        Vector3 currDirV3 = new Vector3(currDir8.x, currDir8.y, 0);
        myAttack.transform.position = this.gameObject.transform.position + ( meleeAttackOffset * currDirV3);
        myAttack.transform.rotation = Quaternion.AngleAxis( MovementUtil.GetRotationAngleForDirection(currDir8), new Vector3(0, 0, 1));
        meleeattackBehaviour.StartAttack();
    }

    private void ProjectileFire()
    {
        levelManager.PlayOneShot(PlayerFire, false);
        Vector3 postion = this.transform.position + FireOffset;

        Quaternion rotation = Quaternion.identity;

        GameObject bullet = Instantiate(Bullet, postion, rotation) as GameObject;
        Rigidbody2D bullet_body = bullet.GetComponent<Rigidbody2D>();
        this.CurrentDirection.Normalize();
        bullet_body.velocity = CurrentDirection * BulletSpeed;
        float smallAngle = MovementUtil.GetRotationAngleForDirection(CurrentDirection);

        bullet_body.rotation = smallAngle;
                                          
    }
    
}
