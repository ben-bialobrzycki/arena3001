﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour {

    public GameObject BackgroundImageObject;
    public GameObject FrontImageObject;

    public RectTransform BackgroundRect;
    public RectTransform FrontRect;
    
    // Use this for initialization
    void Start () {
    
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    public void SetValue(float percentage)
    {
        FrontRect.localScale = new Vector3(  percentage , FrontRect.localScale.y, FrontRect.localScale .z);
    }
}
