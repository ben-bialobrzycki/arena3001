﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombBehaviour : MonoBehaviour
{

    public GameObject BombShadowPreFab;
    public GameObject BombSpritePreFab;
    public GameObject ExplosionPreFab;


    GameObject BombShadow;
    GameObject BombSprite;


    Rigidbody2D body;
    Rigidbody2D bombSpriteBody;

    public float TimeToExplode = 3.0f;

    void Start()
    {
        this.body = GetComponent<Rigidbody2D>();


    }

    IEnumerator RunExplode()
    {
        yield return new WaitForSeconds(TimeToExplode);
        Instantiate(ExplosionPreFab, BombSprite.transform.position, Quaternion.identity);
        Destroy(this.gameObject);

    }

    Vector2 Target;
    float speed = 5.0f;
    public void ThrowToTarget(Vector3 initialPosition, Vector2 target)
    {
        this.body = GetComponent<Rigidbody2D>();
        this.Target = target;
        this.gameObject.SetActive(true);
        this.transform.position = initialPosition;
        BombShadow = Instantiate(BombShadowPreFab, this.transform.position, Quaternion.identity);
        BombShadow.transform.SetParent(this.transform);
        BombSprite = Instantiate(BombSpritePreFab, this.transform.position, Quaternion.identity);
        BombSprite.transform.SetParent(this.transform);
        float time = GetETA();
        VelocityYInitial = (0.5f * AccelerationDueToGravity * time);
        VelocityY = VelocityYInitial;
        Throwing = true;
        StartCoroutine("RunExplode");
    }

    float VelocityYInitial;
    float VelocityY;

    bool Throwing = false;
    float TimeTook;

    float CloseEnough = 0.05f;
    public float AirBorneZoomFactor = 0.1f;

    void Update()
    {
        float distance = Vector2.Distance(this.body.position, this.Target);
        if (Throwing)
            TimeTook += Time.deltaTime;
        if (distance < CloseEnough)
        {
            this.body.velocity = Vector2.zero;
            Throwing = false;
            return;

        }


        VelocityY = VelocityY - (AccelerationDueToGravity * Time.deltaTime);
        Vector3 oldPos = BombSprite.transform.position;
        oldPos.y = oldPos.y + (VelocityY * Time.deltaTime);
        BombSprite.transform.position = oldPos;


        float scaleFac = Vector3.Distance(BombShadow.transform.position, BombSprite.transform.position);
        Vector3 scale = new Vector3(1 + (AirBorneZoomFactor * scaleFac), 1 + (AirBorneZoomFactor * scaleFac), 1);
        BombSprite.transform.localScale = scale;

    }

    public float AccelerationDueToGravity;
    float GetETA()
    {
        float distance = Vector2.Distance(this.transform.position, this.Target);
        Vector2 dirn = Target - body.position;
        dirn.Normalize();
        body.velocity = dirn * speed;
        float t = distance / speed;
        return t;
    }
}
