﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameOverPanelBehaviour : MonoBehaviour {

    public Text highScoreText;
    public Text LastScoreText;
    public Text txtCongrats;
    public GameObject btnRestart;
    public GameObject btnExit;
    Animator aniimator;
    // Use this for initialization
    void Start ()
    {
        aniimator = GetComponent<Animator>();
        Text[] childText = GetComponentsInChildren<Text>(true);
        foreach (Text text in childText)
        {
            if (text.name == "txtOverScore")
                LastScoreText = text;
            if (text.name == "txtOverHighScore")
                highScoreText = text;
            if (text.name == "txtCongrats")
                txtCongrats = text;
        }
#if UNITY_WEBGL
        highScoreText.gameObject.SetActive(false);
        txtCongrats.gameObject.SetActive(false);
#endif
        // Redisplay();
    }

    public void Redisplay()
    {
        LastScoreText.text = "Score: " + LevelManager.CurrentLevel.score;
#if !UNITY_WEBGL
        ScoreEntry lastHighScore = GameManager.GetInstance().Scores.GetCurrentHighScore();

        int highScore = 0;
        if (lastHighScore != null)
            highScore = lastHighScore.Score;
        highScoreText.text = "High Score: " + highScore;
        if (LevelManager.CurrentLevel.score > highScore)
            txtCongrats.gameObject.SetActive(true);
        else
            txtCongrats.gameObject.SetActive(false);
#endif
        aniimator.Play("FrontGateClose");
        
        btnRestart.GetComponent<Button>().Select();
    }


    // Update is called once per frame
    void Update () {
		
	}
}
