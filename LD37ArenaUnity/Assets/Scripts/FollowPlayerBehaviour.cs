﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowPlayerBehaviour : MonoBehaviour
{

    float WalkSpeed = 1.5f;
    LevelManager levelManager;
    Rigidbody2D body;
   
    void Start()
    {
        levelManager = FindObjectOfType(typeof(LevelManager)) as LevelManager;
        this.body = GetComponent<Rigidbody2D>();
    }

    void Update()
    {
        if (!this.levelManager.IsPLayerAlive())
        {
            this.body.velocity = Vector2.zero;
        }
        else
        {
            Vector2 playerPos = this.levelManager.GetPlayerPosition();
            Vector2 dirn = playerPos - this.body.position;
            dirn.Normalize();
            this.body.velocity = dirn * WalkSpeed;
        }
    }
}

