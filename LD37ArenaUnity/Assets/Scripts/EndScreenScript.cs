﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class EndScreenScript : MonoBehaviour {


    public GameObject[] TextItems;
    List<Text> TextUIItems;

    // Use this for initialization
    void Start () {
        TextUIItems = new List<Text>();
        for (var i=0; i < TextItems.Length;i++)
        {
            Text t = TextItems[i].GetComponent<Text>();
            t.color = new Color(t.color.r, t.color.g, t.color.b, 0f);
            TextUIItems.Add(t);
        }
        StartCoroutine("FadeInText", 0);

	}

    IEnumerator FadeInText(int text_index)
    {
        if (TextUIItems.Count <= text_index)
            yield break;
        Text text = TextUIItems[text_index];
        text.color = new Color(text.color.r, text.color.g, text.color.b, 0f);
        yield return new WaitForSeconds(2);
        while (text.color.a < 1.0f)
        {
            yield return new WaitForSeconds(0.01f);
            text.color = new Color(text.color.r,text.color.g,text.color.b, text.color.a + 0.01f);
        }
        StartCoroutine("FadeInText",text_index + 1);
    }

    void Update()
    {

        if (IsFireButtonDown())
        {
            RunGame();
        }
        if (Input.GetKeyDown(KeyCode.Escape))
            Application.Quit();
    }

    bool IsFireButtonDown()
    {
        return Input.GetKeyDown(KeyCode.Space) || Input.GetButtonDown("Fire1");

    }

    void RunGame()
    {
        SceneManager.LoadScene("TitleScreen",LoadSceneMode.Single);
    }
}
