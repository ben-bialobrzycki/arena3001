﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TridentBehaviour : MonoBehaviour {
    public float DamageValue = 1.0f;
  
    SpriteRenderer renderer;
    Rigidbody2D body;
    Animator animator;
    public AudioClip[] TridentCollisionSound;

    void Start()
    {
        renderer = this.gameObject.GetComponent<SpriteRenderer>();
        body = gameObject.GetComponent<Rigidbody2D>();
        //    body.velocity = Vector2.zero;
        animator = gameObject.GetComponent<Animator>();
        if (body.velocity.x < 0)
            renderer.flipX = true; 
    }

    // Update is called once per frame
    void Update()
    {

    }


    public bool IsDestroyedByPlayerProjectile;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        string thisTag = this.gameObject.tag;
        string otherTag = collision.gameObject.tag;
        if (otherTag == "Player" && thisTag == "PlayerProjectile")
            return;
        if (otherTag == "EnemyProjectile" && thisTag == "EnemyProjectile")
            return;
        if (otherTag == "Enemy" && thisTag == "PlayerProjectile")
        {
            var enemy = collision.gameObject.GetComponent<Character>();
            if (enemy != null)
            {
                enemy.TakeDamage(this.DamageValue, true);
            }
        }

        CollisionHelper.TriggerPlayerDamage(this.gameObject,collision);

        if (thisTag == "EnemyProjectile" && otherTag == "PlayerProjectile" && !IsDestroyedByPlayerProjectile)
            return;
        if (this.tag == "EnemyProjectile")
        {
            LeaveDiscardedProjectile();
            return;
        }
        Destroy(this.gameObject);
    }

    

    public void HitGroundEvent()
    {
        body.velocity = Vector2.zero;
        body.angularVelocity = 0f;
      
        renderer.sortingLayerName = "Background";
        renderer.sortingOrder = 1;
    }

    bool discarded = false;
    void LeaveDiscardedProjectile()
    {
        LevelManager.CurrentLevel.PlayOneShot(TridentCollisionSound, false);
        discarded = true;
        this.gameObject.layer = LayerMask.NameToLayer("Dead");
        animator.Play("TridentFall");
        var fadeOut = GetComponent<FadeBehaviour>();
        if (fadeOut != null)
            fadeOut.RunFade();
    }

    public float DeflectionTimeToGround = 1.0f;
    IEnumerator RunDeflectHitGround()
    {
        yield return new WaitForSeconds(DeflectionTimeToGround);
        if (!discarded)
            LeaveDiscardedProjectile();
    }
    public void DeflectDiscard()
    {
        StartCoroutine("RunDeflectHitGround");
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        string thisTag = this.gameObject.tag;
        string otherTag = collision.gameObject.tag;

        if (otherTag == "Player" && thisTag == "EnemyProjectile")
        {
            var player = collision.gameObject.GetComponent<PlayerScript>();
            if (player != null)
            {
                player.TryTakeDamage(this.gameObject);
            }
        }

        if (thisTag == "EnemyProjectile" && otherTag == "PlayerProjectile" && !IsDestroyedByPlayerProjectile)
            return;
        if (this.tag == "EnemyProjectile")
        {
            LeaveDiscardedProjectile();
            return;
        }
    }
}
