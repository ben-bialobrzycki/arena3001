﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


    public delegate void LevelEventFunction(LevelManager manager);

    public class LevelEvent
    {
        public float TimeToWait;
        public LevelEventFunction Levelfunction;
        public LevelEvent(float time, LevelEventFunction func)
        {
            this.TimeToWait = time;
            this.Levelfunction = func;
        }
    }

