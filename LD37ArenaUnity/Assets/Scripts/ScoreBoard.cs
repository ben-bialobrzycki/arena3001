﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;



[Serializable]
public class ScoreBoard
{
    public ScoreBoard()
    {
        Entries = new List<ScoreEntry>();
    }
    public List<ScoreEntry> Entries;
    public const int MaximumSize = 5;
    public bool AddScoreEntry(int score)
    {
        if (Entries == null)
            Entries = new List<global::ScoreEntry>();

        bool found = false;
        if (Entries.Count < MaximumSize)
        {
            Entries.Add(new ScoreEntry() { Score = score, Time = DateTime.Now });
            found = true;
        }
        for (int i=0; i < Entries.Count;i++)
        {
            if (Entries[i].Score < score)
            {
                Entries.Insert(i, new ScoreEntry() { Score = score, Time = DateTime.Now });
                found = true;
                break;
            }
        }
        if (!found)
            return false;
        Entries.Sort((x, y) => y.Score.CompareTo(x.Score)); //Gives Descending Score
        if (Entries.Count > MaximumSize)
            Entries.RemoveRange(MaximumSize - 1, Entries.Count - MaximumSize);
        return true;
    }

    public ScoreEntry GetCurrentHighScore()
    {
        if (Entries.Count > 0)
            return Entries[0];
        return null;
    }
}

[Serializable]
public class ScoreEntry
{
    public int Score;
    public DateTime Time;
    public string UserName;
}


