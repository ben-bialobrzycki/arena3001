﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class MouseInputBehaviour : MonoBehaviour {


	// Use this for initialization
	void Start () {
		
	}
	

	// Update is called once per frame
	void Update () {
     //   if (EventSystem.current.IsPointerOverGameObject())
     //       return;
        this.gameObject.transform.position = Input.mousePosition;
    
    }

    public RaycastHit2D GetWorldPosition()
    {
        Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
        //   RaycastHit2D hit = Physics2D.Raycast(ray.origin, ray.direction, Mathf.Infinity, LayerMask.NameToLayer("RaycastTarget"));
        RaycastHit2D hit = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);//, LayerMask.NameToLayer("RaycastTarget"));

        return hit;
    }
}
