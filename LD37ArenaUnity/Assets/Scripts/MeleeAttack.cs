﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MeleeAttack : MonoBehaviour {

    InflictDamage DamageInflictor;
    public float Duration = 0.5f;
    float actualDuration ;
    float timer;
    public bool IsAttacking;
    Collider2D collider2D;
    SpriteRenderer spriteRenderer;
    Animator animator;
	// Use this for initialization
	void Start () {
        this.DamageInflictor = GetComponent<InflictDamage>();
        this.IsAttacking = false;
        this.timer = -1;
        actualDuration = Duration;
        this.spriteRenderer = GetComponent<SpriteRenderer>();
        collider2D = GetComponent<Collider2D>();
        animator = GetComponent<Animator>();
        UpdateForAtcking();
	}
	

    public void SetDuration(float newDuration)
    {
        this.actualDuration = newDuration;
    }
  
	// Update is called once per frame
	void Update () {
        if (timer > 0)
            timer -= Time.fixedDeltaTime;
        if (timer < 0 && IsAttacking)
        {
            IsAttacking = false;
            //if (!this.IsAttacking)
            UpdateForAtcking();
        }

    }

    void UpdateForAtcking()
    {
        spriteRenderer.enabled = this.IsAttacking;
        collider2D.enabled = this.IsAttacking;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        string thisTag = this.gameObject.tag;
        string otherTag = collision.gameObject.tag;
        //  Debug.LogFormat("Called On Trigger Enter this {0} Collider2d {1}", this.gameObject.tag, collision.gameObject.tag);
        if (otherTag == TagUtil.Player && thisTag == TagUtil.PlayerProjectile)
            return;
        if (otherTag == TagUtil.EnemyProjectile && thisTag == TagUtil.EnemyProjectile)
            return;
        if (thisTag == TagUtil.PlayerProjectile)
        {
            if (otherTag == TagUtil.Enemy)
            {
                var enemy = collision.gameObject.GetComponent<Character>();
                if (enemy != null)
                {
                    enemy.TakeDamage(this.DamageInflictor.DamageValue, true);
                }

                var knockback = collision.gameObject.GetComponent<KnockBackBehaviour>();
                if (knockback != null)
                    knockback.TakeKnockBack(this.gameObject.transform.position, EnemyKnockBackForce);
                else
                    Debug.Log("Should have had knockback");
            }
         
            if (otherTag == TagUtil.EnemyProjectile)
            {
                var deflector = collision.gameObject.GetComponent<DeflectBehaviour>();
                if (deflector != null)
                    deflector.TakeDeflection(this.transform.position, GetKnockBackForce(thisTag, otherTag));
            }
        }


        if (otherTag == "Player" && thisTag == "EnemyProjectile")
        {
            var player = collision.gameObject.GetComponent<PlayerScript>();
            if (player != null)
            {
                player.TryTakeDamage(this.gameObject);
            }
        }

     //   if (thisTag == "EnemyProjectile" && otherTag == "PlayerProjectile" && !IsDestroyedByPlayerProjectile)
      //      return;
        //if (this.tag == "EnemyProjectile" )
        //{
        //    LeaveDiscardedProjectile();
        //    return;
        //}
     //   Destroy(this.gameObject);
    }


    public void StartAttack()
    {
        IsAttacking = true;


        
        animator.Play("SwordSlash",0,0);
      //  animator.playbackTime = 0.0f;
      //  var stateInfo = animator.GetCurrentAnimatorStateInfo(0);
      //  AnimationState state;
       // state.time = 0;
       // var stateInfo = animator.GetCurrentAnimatorStateInfo(0);
       

        UpdateForAtcking();
        timer = Duration;

    }

    public float EnemyKnockBackForce = 1000.0f;
    float GetKnockBackForce(string thisTag, string otherTag)
    {
       return EnemyKnockBackForce;
    }

    public void FinishAttackEvent()
    {
        IsAttacking = false;
        UpdateForAtcking();
    }
    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (!this.IsAttacking)
            return;

        string thisTag = this.gameObject.tag;
        string otherTag = collision.gameObject.tag;
       
        if (otherTag == "Player" && thisTag == "PlayerProjectile")
            return;
        if (otherTag == "EnemyProjectile" && thisTag == "EnemyProjectile")
            return;

        if (thisTag == "PlayerProjectile")
        {
            if (otherTag == "Enemy")
            {
                var enemy = collision.gameObject.GetComponent<Character>();
                if (enemy != null)
                {
                    enemy.TakeDamage(this.DamageInflictor.DamageValue, true);
                }

                var knockback = collision.gameObject.GetComponent<KnockBackBehaviour>();
                if (knockback != null)
                    knockback.TakeKnockBack(this.gameObject.transform.position, EnemyKnockBackForce);
                else
                    Debug.Log("Should have had knockback");
            }

        }


        if (otherTag == "Player" && thisTag == "EnemyProjectile")
        {
            var player = collision.gameObject.GetComponent<PlayerScript>();
            if (player != null)
            {
                player.TryTakeDamage(this.gameObject);
            }
        }

      

      
    }
}
