﻿using System.Collections;
using System.Collections.Generic;
using System.Text;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TitleScreenBehaviour : MonoBehaviour {

    public GameObject[] MenuItems;
    public GameObject BackgroundImage;
    public GameObject pnlScores;

    int selectedItem = 0;

    bool runningGame = false;
    public GameObject txtScoreText;
    public GameObject pnlControls;
    public GameObject pnlMain;

    public GameObject btnControlMouse;
    public GameObject btnControlTwinStick;
    public GameObject btnControlClassic;
    public GameObject btnScores;
    // Use this for initialization
    void Start () {

        var manager = GameManager.GetInstance();
        string scores = "";
        if (manager.Scores != null)
        {
            StringBuilder sb = new StringBuilder();
            foreach (var s in manager.Scores.Entries)
            {
                sb.Append(s.UserName);
                sb.Append("     ");
                sb.Append(s.Score);
                sb.Append("   ");
                sb.Append(s.Time.ToShortDateString());
                sb.AppendLine();
            }
            scores = sb.ToString();
        }
        txtScoreText.GetComponent<Text>().text = scores;
      
        ShowHideScores();
         runningGame = false;
        SetOptionControlButtons();
#if UNITY_WEBGL
        btnScores.SetActive(false);
#endif

    }
	

    void SetSelectedControlOptionForCurrent()
    {
        var options = GameManager.GetInstance().GameOptions;
        Button btn = null;
        switch (options.ControlScheme)
        {
            case InputScheme.Classic: btn = btnControlClassic.GetComponent<Button>(); break;
            case InputScheme.Mouse: btn = btnControlMouse.GetComponent<Button>(); break;
            case InputScheme.Twinstick: btn = btnControlTwinStick.GetComponent<Button>(); break;
        }
        if (btn != null)
            btn.Select();

    }
    void SetOptionControlButtons()
    {
        var options = GameManager.GetInstance().GameOptions;
        btnControlClassic.GetComponent<Button>().onClick.AddListener(delegate () {
            options.ControlScheme = InputScheme.Classic;
        });
        btnControlMouse.GetComponent<Button>().onClick.AddListener(delegate () {
            options.ControlScheme = InputScheme.Mouse;
        });
        btnControlTwinStick.GetComponent<Button>().onClick.AddListener(delegate () 
        {
            options.ControlScheme = InputScheme.Twinstick;
        });
  
    }
    
	// Update is called once per frame
	void Update () {
   
    }


    public void StartNormalMode()
    {
        GameManager.GetInstance().GameOptions.Mode = GameMode.Normal;
        RunGame();
    }

    public void StartSurvivalMode()
    {
        GameManager.GetInstance().GameOptions.Mode = GameMode.Survival;
        RunGame();
    }

    public void StartExit()
    {
        Application.Quit();
    }

    public void OptionsClick()
    {
        pnlMain.SetActive(false);
        pnlControls.SetActive(true);
        SetSelectedControlOptionForCurrent();
    }

    

    public void OptionBackClick()
    {
        pnlMain.SetActive(true);
        pnlControls.SetActive(false);
    }



    bool IsFireButtonDown()
    {
        return Input.GetKeyDown(KeyCode.Space) || Input.GetButtonDown("Fire1");

    }

    bool scoresOn = false;
    public void ToggleScores()
    {
        scoresOn = !scoresOn;
        ShowHideScores();
    }

    void ShowHideScores()
    {
        pnlScores.SetActive(scoresOn);
        BackgroundImage.SetActive(!scoresOn);

    }
    
    void RunGame()
    {
        if (runningGame)
            return;

        SceneManager.LoadScene("MainScene",LoadSceneMode.Single);
        runningGame = true;
    }
}
