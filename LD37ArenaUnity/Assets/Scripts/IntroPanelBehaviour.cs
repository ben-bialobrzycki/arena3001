﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IntroPanelBehaviour : MonoBehaviour {

    Animator animator;
	// Use this for initialization
	void Start () {
        animator = GetComponent<Animator>();
	}

    public void PlayIntro()
    {
        if (animator == null)
            animator = GetComponent<Animator>();
        animator.Play("IntroAnimationClip",0,0);
    }
	// Update is called once per frame
	void Update () {
		
	}

    public void FinishEvent()
    {
        this.gameObject.SetActive(false);
    }
}
