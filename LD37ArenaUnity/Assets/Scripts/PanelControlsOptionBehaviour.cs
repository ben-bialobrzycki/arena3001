﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PanelControlsOptionBehaviour : MonoBehaviour {

    public Button btnMouseControl;
    public Button btnClassicControl;
    public Button btnTwinStickControl;
    // Use this for initialization
    void Start () {

        EnsureButtonsSet();

    }
	
    void EnsureButtonsSet()
    {
        if (btnClassicControl != null)
            return;
        Button[] buttons = GetComponentsInChildren<Button>(true);
        foreach (Button b in buttons)
        {
            switch (b.name)
            {
                case "btnClassic": btnClassicControl = b; break;
                case "btnTwinStick": btnTwinStickControl = b; break;
                case "btnMouse": btnMouseControl = b; break;
            }


        }
    }
    public void SetCurrentlySelected(InputScheme inputScheme)
    {
        EnsureButtonsSet();

        switch (inputScheme)
        {
            case InputScheme.Classic:btnClassicControl.Select();break;
            case InputScheme.Mouse: btnMouseControl.Select(); break;
            case InputScheme.Twinstick: btnTwinStickControl.Select(); break;
        }

    }


	// Update is called once per frame
	void Update () {
		
	}
}
