﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;


    public class Range<T>
    {
        public float Low;
        public float High;
        public T GameObjectPreFab;
    }

public class ChanceSet<T>
{
    public ChanceSet()
    {
        ChanceRanges = new List<Range<T>>();
    }
    List<Range<T>> ChanceRanges;
    public void Add(Range<T> range)
    {
        ChanceRanges.Add(range);
    }

    public void Build(float[] TierChance, T[] TierEnemyPreFab)
    {
        float bottom = 0;
        float top = 0;
        for (int i = 0; i < TierChance.Length; i++)
        {
            top += TierChance[i];
            Range<T> r = new Range<T>();
            r.Low = bottom;
            r.High = top;
            bottom += TierChance[i];
            r.GameObjectPreFab = TierEnemyPreFab[i];
            ChanceRanges.Add(r);


        }
    }

    public T GetRandom()
    {
        float TotalChance = ChanceRanges[ChanceRanges.Count - 1].High;
        float enemChance = UnityEngine.Random.Range(0, TotalChance);
        foreach (Range<T> r in ChanceRanges)
        {
            if (r.Low <= enemChance && enemChance <= r.High)
                return r.GameObjectPreFab;
        }
        return default(T);
    }
}


