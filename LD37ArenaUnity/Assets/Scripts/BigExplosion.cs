﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BigExplosion : MonoBehaviour {

    public float ExplosionTime = 4.0f;
    public float ExplosionFreq = 0.25f;
    public int NumberOfExplosions = 2;
    public float RangeX = 1.0f;
    public float RangeY = 1.0f;
    public GameObject PreFabExplosion;
    float TimeDone = 0f;
    // Use this for initialization
    void Start()
    {
        TimeDone = 0f;
        RunExplosion();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    IEnumerator CreateExplosions()
    {

        while (TimeDone < ExplosionTime)
        {
            CreateOneExplosion();
            yield return new WaitForSeconds(ExplosionFreq);
            TimeDone += ExplosionFreq;

        }
    }

    public void RunExplosion()
    {
        StartCoroutine("CreateExplosions");
    }

    void CreateOneExplosion()
    {
        GameObject scorps = PreFabExplosion;
        float x = Random.Range(-RangeX, RangeX);
        float y = Random.Range(-RangeY, RangeY);

        Vector3 offset = new Vector3(x, y, 0);
        Instantiate(scorps, (this.transform.position + offset), Quaternion.identity);
    }
}
