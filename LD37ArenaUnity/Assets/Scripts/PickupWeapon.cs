﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupWeapon : MonoBehaviour {

    public PlayerWeapon Weapon;

    public Sprite SwordSprite;
    public Sprite GunSprite;

    SpriteRenderer spriteRenderer;
    // Use this for initialization
    void Start () {
        this.spriteRenderer = GetComponent<SpriteRenderer>();
        if (weaponSprite != null)
            this.spriteRenderer.sprite = weaponSprite;
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    Sprite weaponSprite;

    public void SetWeapon(PlayerWeapon weapon)
    {
        this.Weapon = weapon;
        weaponSprite = null;
        switch (this.Weapon)
        {
            case PlayerWeapon.SimpleGun:
                weaponSprite = GunSprite; break;
            case PlayerWeapon.Sword:
                weaponSprite = SwordSprite;break;

        }
        if (weaponSprite != null && this.spriteRenderer != null)
            this.spriteRenderer.sprite = weaponSprite;
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag == "Player")
        {
            var player = collision.gameObject.GetComponent<PlayerScript>();
            if (player != null)
            {
                player.SetWeapon(this.Weapon);
            }
            Destroy(this.gameObject);
        }


    }
}
