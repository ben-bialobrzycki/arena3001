﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Character : MonoBehaviour {

    public float Health = 10;
    public int PointValue = 10;
    LevelManager levelManager;
    [HideInInspector]
    public bool Immune;

    [HideInInspector]
    public IDamageSubscriber DamageSubscriber;

    public bool SupportsTemporaryImmunity = false;
    public bool DestroyOnDie = true;
    public float MaximumHealth;
	// Use this for initialization
	void Start () {
        levelManager = FindObjectOfType(typeof(LevelManager)) as LevelManager;
        this.Health = MaximumHealth;

    }
	
	// Update is called once per frame
	void Update () {
		
	}

    void CreateExplosion()
    {
        GameObject scorps =  levelManager.ExplosionPreFab;
        Instantiate(scorps, this.transform.position, Quaternion.identity);

    }

    public bool IsDead()
    {
        return Health <= 0;
    }

    public void TakeDamage(float damageValue, bool DoExplosion)
    {
        if (Immune)
            return;
        this.Health -= damageValue;
 
        if (this.Health <= 0)
            RunDie(DoExplosion);
        else
        {
            if (DamageSubscriber != null)
                DamageSubscriber.TakeDamage(damageValue);
            RunTemporaryImmunity();
        }
       
    }

    float ImmunityResetTime = 1.0f;
    IEnumerator ImmunityReset()
    {
        Immune = true;
     
        yield return new WaitForSeconds(ImmunityResetTime);
        Immune = false;
    }

    public float PickupDropTime = 1.0f;
 
    public void RunTemporaryImmunity()
    {
        if (SupportsTemporaryImmunity)
        {
            StartCoroutine("ImmunityReset");
        }
    }
    public void RunDie(bool DoExplosion)
    {
        levelManager.UpdateScore(this.PointValue);
        if (DoExplosion)
        {
            var specialExplosion = this.GetComponent<SpecialExplosion>();
            if (specialExplosion != null)
                specialExplosion.RunExplosion();
            else
            {
                CreateExplosion();
            }
        }
        levelManager.EnemyDie(this.gameObject);
 
        this.gameObject.SetActive(false);
        if (DestroyOnDie)
            Destroy(this.gameObject);
    }

    internal void AddHealth(float healthValue)
    {
        this.Health += healthValue;
        if (this.Health > MaximumHealth)
            this.Health = MaximumHealth;
    }
}
