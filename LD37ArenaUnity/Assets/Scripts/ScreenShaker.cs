﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScreenShaker : MonoBehaviour {

    bool ScreenShaking;
    Camera maincamera;
    Vector3 initialCamera;
    public void DoScreenShake()
    {
        //Mathf.si
    }

    float timeRunning;

    private void Init()
    {
        if (!running)
          initialCamera = maincamera.transform.position;
        timeRunning = 0f;
    }
    public void Run()
    {
        Init();
        running = true;
       // StartCoroutine("Stop");

    }
    // Use this for initialization
    void Start () {
        maincamera = Camera.main;
        initialCamera = maincamera.transform.position;
        Init();
	}

    bool running;
    IEnumerator Stop()
    {
        yield return new WaitForSeconds(Duration);
        Finish();
    }

    void Finish()
    {
        running = false;
        maincamera.transform.position = initialCamera;
    }

    public float Duration = 1.0f;
    public float SpeedX = 0.1f;
    public float SpeedY = 0.1f;
    public float AmplitudeX = 1.0f;
    public float AmplitudeY = 1.0f;
    public float Rampdown = 1.0f;

    // Update is called once per frame
    void Update () {
        if (timeRunning > Duration)
            Finish();
        if (!running)
            return;
        timeRunning += Time.deltaTime;
        float rampFactor;
        
        rampFactor = 1 - (timeRunning / Duration); // 1.0f / (1.0f + (Rampdown * timeRunning)); 
        float amountx =  rampFactor * AmplitudeX * Mathf.Sin(SpeedX * timeRunning * Mathf.PI);
        float amounty = rampFactor * AmplitudeY * Mathf.Sin(SpeedY * timeRunning * Mathf.PI);

        Vector3 ofset = new Vector3(amountx, amounty, 0);
        maincamera.transform.position = initialCamera + ofset;


    }
}
