﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


    public interface IDamageSubscriber
    {
        void TakeDamage(float damage);
    }

