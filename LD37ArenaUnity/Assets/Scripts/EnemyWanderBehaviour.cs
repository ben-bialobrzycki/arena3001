﻿//using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyWanderBehaviour : MonoBehaviour, IDamageSubscriber {

    float WalkSpeed = 1.5f;
    LevelManager levelManager;
    Rigidbody2D body;
    Animator animator;
    public float DamageValue = 5.0f;
    Action CurrentAction;
    Action PreviousAction;
    public GameObject BulletPreFab;
    public float BulletSpeed = 3.0f;
    enum Action { None,Taunt,Attack,Walk};
    Character characterbehaviour;
    SpriteRenderer spriteRenderer;
    KnockBackBehaviour knockbackbehaviour;
    FallBehaviour fallbehaviour;
    public AudioClip[] TauntClip;
    public AudioClip[] AttackClip;

    public string WalkAnimationName;
    public string TauntAnimationName;
    public string AttackAnimationName;

    public bool IsBombThrower = false;

    Vector2 velocity;
    public float AttackLaunchTime = 0.5f;

    // Use this for initialization
    void Start()
    {
        levelManager = FindObjectOfType(typeof(LevelManager)) as LevelManager;
        this.animator = GetComponent<Animator>();
        this.body = GetComponent<Rigidbody2D>();
        this.characterbehaviour = GetComponent<Character>();
        this.knockbackbehaviour = GetComponent<KnockBackBehaviour>();
        this.characterbehaviour.DamageSubscriber = this;
        this.fallbehaviour = GetComponent<FallBehaviour>();
        this.spriteRenderer = GetComponent<SpriteRenderer>();
        StartWalk();

    }

    Action GetRandomAction()
    {
        //TODO refactor to use ChanceRange stuf rather than hard coded
        float rand = Random.Range(0, 100);
        if (0 < rand && rand < 20 && PreviousAction != Action.Taunt )
        {
             return Action.Taunt;
        }
        if (20 < rand && rand < 60)
        {
            return Action.Walk;
        }
        if (60 < rand && rand < 100 && PreviousAction != Action.Attack)
        {
            return Action.Attack;
        }
       
        return Action.Walk;
    }

    void ResetAction()
    {
        PreviousAction = this.CurrentAction;
        this.CurrentAction = Action.None;
    }

    IEnumerator SwitchAction(float time)
    {
        yield return new WaitForSeconds(time);
        ResetAction();
    }

    void StartTaunt()
    {
        this.animator.Play(TauntAnimationName, 0, 0);
        this.velocity = Vector2.zero;
        levelManager.PlayOneShot(TauntClip);
        StartCoroutine("SwitchAction", 1);
    }


        IEnumerator FinishAttack()
    {
        yield return new WaitForSeconds(AttackLaunchTime);
        FinishAttackEvent();
    }

    void FinishAttackEvent()
    {
        DoFire();
        PreviousAction = this.CurrentAction;
        this.CurrentAction = Action.None;
    }
     void StartAttack()
    {
        this.animator.Play(AttackAnimationName, 0, 0);
        this.velocity = Vector2.zero;
      //  this.animator.SetTrigger("DoAction");
    }

    void DoFire()
    {

        if (!levelManager.IsPLayerAlive())
            return;
        levelManager.PlayOneShot(AttackClip);

        if (this.IsBombThrower)
            FireBomb();
        else
            FireTrident();

    }

    public float BombRandom = 1.0f;
    void FireBomb()
    {
        Vector2 playerPos = levelManager.GetPlayerPosition();
        float x = Random.Range(-BombRandom, BombRandom);
        float y = Random.Range(-BombRandom, BombRandom);
        Vector2 target = playerPos + new Vector2(x, y);
        GameObject bomb = Instantiate(BulletPreFab, this.transform.position, Quaternion.identity);
        bomb.GetComponent<BombBehaviour>().ThrowToTarget(this.transform.position, target);
    }

    private void FireTrident()
    {
        Vector3 postion = this.transform.position + new Vector3(0.5f, 0.5f, 0);

        Quaternion rotation = Quaternion.identity;

        GameObject bullet = Instantiate(BulletPreFab, postion, rotation) as GameObject;
        Rigidbody2D bullet_body = bullet.GetComponent<Rigidbody2D>();
        Vector2 fireDirn = levelManager.GetPlayerPosition() - body.position;
        fireDirn.Normalize();
        bullet_body.velocity = fireDirn * BulletSpeed;
        float smallAngle = Vector2.Angle(Vector2.up, fireDirn);
        if (fireDirn.x > 0)
            smallAngle = 360 - smallAngle;
        bullet_body.rotation = smallAngle;
    }

    //public void OnTriggerEnter2D(Collider2D collision)
    //{
    //    if (collision.gameObject.tag == "Player")
    //    {
    //        return;
    //    }
    //    if (collision.gameObject.tag == "Enemy")
    //    {
    //        var enemy = collision.gameObject.GetComponent<Enemy>();
    //        if (enemy != null)
    //        {

    //            ;// this.characterBehaviour.TakeDamage(enemy.DamageValue);
    //        }
    //    }

    //}


    public void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "Player")
            return;
        if (collision.gameObject.tag == "Enemy")
        {
            var enemy = collision.gameObject.GetComponent<Enemy>();
            if (enemy != null)
            {

                ;//   this.characterBehaviour.TakeDamage(enemy.DamageValue);
            }
        }

    }

    void SetNewAction()
    {
        CurrentAction = GetRandomAction();
        if (CurrentAction == Action.Walk)
        {
            StartWalk();
        }
        if (CurrentAction == Action.Taunt)
        {
            StartTaunt();
        }
        if (CurrentAction == Action.Attack)
        {
            StartAttack();
            
        }
    }

    void Update()
    {
        if (CurrentAction == Action.None)
        {
            SetNewAction();
        }
        else if (CurrentAction == Action.Walk)
        {
            DoWalk();
        }

        if (!this.levelManager.IsPLayerAlive())
        {
            this.velocity = Vector2.zero;
            return;
        }
        
    }

    Vector2 TargetWalk;
    void StartWalk()
    {
        this.animator.Play(WalkAnimationName, 0, 0);
        Vector2 playerPos;
        if (this.levelManager.IsPLayerAlive())
        {
            playerPos = this.levelManager.GetPlayerPosition();
        }
        else
            playerPos = this.levelManager.PlayerStart.position;
        this.TargetWalk = playerPos;
        StartCoroutine("SwitchAction", 5.0f);
    }

    void DoWalk()
    {
        if (this.body.OverlapPoint(TargetWalk))
        {
            StopCoroutine("SwitchAction");
            ResetAction();
            return;
        }
        Vector2 dirn = TargetWalk - this.body.position;
        dirn.Normalize();

        if (!knockbackbehaviour.OnKnockBack && !fallbehaviour.IsFalling)
            this.velocity = dirn * WalkSpeed;
   
    }


    public float DamageFlashTime = 0.3f;
    public Color DamageColor;
    IEnumerator ResetAfterDamage()
    {
        yield return new WaitForSeconds(DamageFlashTime);
        this.spriteRenderer.color = Color.white;
    }
    public void TakeDamage(float damage)
    {
        this.spriteRenderer.color = DamageColor;
        StartCoroutine("ResetAfterDamage");
    }

    public void TakeFall(GameObject sourceObject)
    {
        this.gameObject.SetActive(false);
    }

    private void FixedUpdate()
    {
        this.body.MovePosition(this.body.position + (velocity * Time.deltaTime));
    }

}
