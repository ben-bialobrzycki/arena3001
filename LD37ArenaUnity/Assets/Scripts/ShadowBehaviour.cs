﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShadowBehaviour : MonoBehaviour {

    public GameObject ShadowPreFab;
    public Vector2 Offset = new Vector2(0,-0.5f);
    GameObject myShadow;
	// Use this for initialization
	void Start () {
        myShadow = GameObject.Instantiate(ShadowPreFab, Vector2.zero, Quaternion.identity); 
	}
	
	// Update is called once per frame
	void Update () {
        myShadow.transform.position = gameObject.transform.position + new Vector3( Offset.x, Offset.y);
    }

    private void OnDestroy()
    {
        GameObject.Destroy(myShadow);
    }



}
