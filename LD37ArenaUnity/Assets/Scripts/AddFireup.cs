﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddFireup : MonoBehaviour {

    public float EffectAmount = 2.0f;
    public float EffectDuration = 10.0f;
	
	void Start () {
		
	}
	
	
	void Update () {
		
	}

    public void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Player")
        {
            var player = collision.gameObject.GetComponent<PlayerScript>();
            if (player != null)
            {
                player.AddFireup(this.EffectAmount,this.EffectDuration);
            }
            Destroy(this.gameObject);
        }

      
    }
}
