﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Explosion : MonoBehaviour {

    public float explosionLifeTime = 1.0f;
    public AudioClip ExplosionSound;
    LevelManager levelManager;

    IEnumerator FadeOut()
    {

        yield return new WaitForSeconds(explosionLifeTime);
        this.gameObject.SetActive(false);
        Destroy(this.gameObject);

    }

    // Use this for initialization
    void Start () {
        levelManager = FindObjectOfType(typeof(LevelManager)) as LevelManager;
        levelManager.PlayExplosion();
        StartCoroutine(FadeOut());

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
