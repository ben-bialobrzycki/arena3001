﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthHeart : MonoBehaviour {

    public GameObject FullHeart;
    public GameObject HalfHeart;
    Image spriteImage;
    
    public int Position = 0;
	// Use this for initialization
	void Start () {
        spriteImage = GetComponent<Image>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void Display(int health)
    {
        if (health >= 2)
        {
            this.gameObject.SetActive(true);
            spriteImage.sprite = FullHeart.GetComponent<SpriteRenderer>().sprite;
        }
        else if (health == 1)
        {
            this.gameObject.SetActive(true);
            spriteImage.sprite = HalfHeart.GetComponent<SpriteRenderer>().sprite;
        }
        else
            this.gameObject.SetActive(false);
    }
}
