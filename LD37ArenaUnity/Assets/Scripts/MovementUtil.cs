﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

namespace Assets.Scripts
{
    public class MovementUtil
    {
        public static float GetRotationAngleForDirection(Vector2 direction)
        {
            float smallAngle = Vector2.Angle(Vector2.up, direction);
            if (direction.x > 0)
                smallAngle = 360 - smallAngle;
            return smallAngle;
        }

        public static Vector2 RandomUnitVector()
        {
            Vector2 offset = new Vector2(UnityEngine.Random.Range(-1, 1), UnityEngine.Random.Range(-1, 1));
            offset.Normalize();
            return offset;
        }
    }
}
