﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickupBeahviour : MonoBehaviour
{
    public AudioClip PickUpSound;
    LevelManager levelManager;
    
    void Start()
    {
        levelManager = FindObjectOfType(typeof(LevelManager)) as LevelManager;
        var fade = GetComponent<FadeBehaviour>();
        if (fade != null)
            fade.RunFade();
    }

    public void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag != "Player")
            return;

        if (collision.gameObject.tag == "Player")
        {
            var player = collision.gameObject.GetComponent<PlayerScript>();
            if (player != null)
            {
                levelManager.PlayOneShot(this.PickUpSound, false);
            }
        }

        StopAllCoroutines();
        Destroy(this.gameObject);
    }
}
