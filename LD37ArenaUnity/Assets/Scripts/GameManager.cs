﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

public class GameManager {//: MonoBehaviour {



    static GameManager _instance;

    public GameOptions GameOptions;
    public ScoreBoard Scores;
    public static GameManager GetInstance()
    {
        if (_instance == null)
        {
            Debug.Log("Game Manager instance created");
            _instance = new global::GameManager();
            _instance.Initialise();
        }
        return _instance;
    }

    public void Initialise()
    {
        GameOptions = new GameOptions();
        Load();
    }
	// Use this for initialization
	//void Awake () {
 //       if (_instance == null)
 //       {
 //           DontDestroyOnLoad(gameObject);
 //           _instance = this;
 //       }
 //       else if (_instance != this)
 //           Destroy(this.gameObject);
		
	//}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void UpdateScoreBoard(int score)
    {
#if UNITY_WEBGL
        return;
#else
        if (Scores.AddScoreEntry(score))
            Save();
#endif
    }

    string GetSaveFilePath()
    {
        return Application.persistentDataPath + "/ArenaInfo.dat";
    }
    public void Save()
    {
        BinaryFormatter formatter = new BinaryFormatter();
        string path = GetSaveFilePath();
        FileStream fs = File.Open(path, FileMode.OpenOrCreate);
        formatter.Serialize(fs, Scores);
    }

    public void Load()
    {
#if UNITY_WEBGL
        Scores = new ScoreBoard();
        return;
#else
        string filePath = GetSaveFilePath();
        if (File.Exists(filePath))
        {
            BinaryFormatter bf = new BinaryFormatter();
            FileStream file = File.Open(filePath, FileMode.Open);
            Scores = (ScoreBoard)bf.Deserialize(file);
            file.Close();
        } 
        else
        {
            Scores = new ScoreBoard();
        }
#endif
    }
}
