﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;


    public class TagUtil
    {
        public const string PlayerProjectile = "PlayerProjectile";
        public const string EnemyProjectile = "EnemyProjectile";
        public const string Enemy = "Enemy";
        public const string Wall = "Wall";
        public const string Player = "Player";
        public const string Hazard = "Hazard";
}

