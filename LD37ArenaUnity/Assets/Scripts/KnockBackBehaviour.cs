﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnockBackBehaviour : MonoBehaviour
{

    public float KnockbackForce = 1.0f;
    Rigidbody2D body;
    Character characterInfo;
    public float KnockBackTime = 1.0f;
    // Use this for initialization

    [HideInInspector]
    public bool OnKnockBack;
    void Start()
    {
        Debug.Log("Start called on knockbackbehaviour");
        body = GetComponent<Rigidbody2D>();
        characterInfo = GetComponent<Character>();
        OnKnockBack = false;

    }

    public void TakeKnockBack(Vector3 fromPosition, float force)
    {
        //return;
        if (characterInfo != null && characterInfo.Immune)
            return;
        if (!this.gameObject.activeSelf)
            return;
        Vector2 dir = this.body.transform.position - fromPosition;
        dir.Normalize();
        float appliedForce;
        if (force >= 0)
            appliedForce = force;
        else
            appliedForce = KnockbackForce;
        body.AddForce(dir * appliedForce, ForceMode2D.Force);
        if (OnKnockBack)
            StopCoroutine("KnockbackReset");
        StartCoroutine("KnockbackReset");
        Debug.Log("took knockback");
    }

    IEnumerator KnockbackReset()
    {
        OnKnockBack = true;
        yield return new WaitForSeconds(KnockBackTime);
        OnKnockBack = false;
    }
    // Update is called once per frame
    void Update()
    {

    }
}
