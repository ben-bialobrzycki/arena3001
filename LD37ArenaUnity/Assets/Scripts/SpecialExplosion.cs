﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpecialExplosion : MonoBehaviour {


    public GameObject PreFabExplosion;

    public void RunExplosion()
    {
        GameObject scorps = PreFabExplosion;
       
        Instantiate(scorps, (this.transform.position), Quaternion.identity);
    }

  
}
