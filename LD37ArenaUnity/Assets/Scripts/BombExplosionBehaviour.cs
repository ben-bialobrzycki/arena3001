﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BombExplosionBehaviour : MonoBehaviour {

    public AudioClip ExplosionSound;
    CircleCollider2D mycollider;
    public GameObject ExplosionParticlePreFab;
    
	// Use this for initialization
	void Start () {
      
        LevelManager.CurrentLevel.PlayOneShot(ExplosionSound, true);
        /*doesn't look so good*/
       // if (ExplosionParticlePreFab != null)
       // GameObject.Instantiate(ExplosionParticlePreFab, this.transform.position, Quaternion.identity); 
        var shaker = Camera.main.GetComponent<ScreenShaker>();
        if (shaker != null)
           shaker.Run();
        else
            Debug.Log("No screen shaker");
        mycollider = GetComponent<CircleCollider2D>();
    }
	
	// Update is called once per frame
	void Update () {
		
	}

    private void OnTriggerEnter2D(Collider2D collision)
    {
        CollisionHelper.TriggerPlayerDamage(this.gameObject, collision);
    }

    public void ExplosionDamageFinish()
    {
        mycollider.enabled = false;
    }

    public void ExplosionRemove()
    {
        Destroy(this.gameObject);
    }
}
