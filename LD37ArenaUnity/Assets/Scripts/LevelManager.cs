﻿
//using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelManager : MonoBehaviour
{


    public GameOptions GameOptions;
    public GameObject ExplosionPreFab;
    public GameObject PlayerPreFab;
    public GameObject BlueLionPreFab;
    public GameObject EnemyTridentPreFab;
    public GameObject FrontGatePreFab;
    public GameObject SideGateLeftPreFab;
    public GameObject SideGateRightPreFab;
    public GameObject BottomGatePreFab;
    public GameObject HealthPickUpPreFab;
    public GameObject WeaponPickUpPreFab;
    public GameObject TrapDoorPreFab;

    public GameObject GameOverPanel;
    public GameObject GameOverPanelButtons;
    public GameObject GameOverPanelButtonRestart;
    public GameObject GameOverPanelButtonExit;
    public GameObject IntroPanel;

    public GameObject[] PickupPrefabs;
    public float[] PickupChance;

    public GameObject EmperorPreFab;

    public GameObject BossHealthBar;

    public Transform PlayerStart;
    public Transform FrontGate1;
    public Transform FrontGate2;
    public Transform LeftGate1;
    public Transform LeftGate2;
    public Transform RightGate1;
    public Transform RightGate2;
    public Transform BottomGate1;
    public Transform BottomGate2;

    public Transform TrapDoor1;
    public Transform TrapDoor2;

    public Transform TrapDoor3;
    public Transform TrapDoor4;

    public GameObject txtGameOver;
    public GameObject txtScore;
    public GameObject txtHealth;
    public GameObject txtWaveText;

    public AudioSource efxSource;
    public AudioSource musicSource;

    public int StartLevel = 1;
    PlayerScript player;

    GameObject FrontGateLeft;
    GameObject FrontGateRight;

    GameObject SideGateTopLeft;
    GameObject SideGateBottomLeft;

    GameObject SideGateTopRight;
    GameObject SideGateBottomRight;

    GameObject BottomGateLeft;
    GameObject BottomGateRight;




    GameObject TrapTopLeft;
    GameObject TrapTopRight;

    GameObject TrapBottomLeft;
    GameObject TrapBottomRight;

    List<TrapDoorBehaviour> TrapDoors;
    public AudioClip PlayerFire;
    public AudioClip EnemyFire;
    public AudioClip EnemyTaunt;
    public AudioClip PlayerTakeDamage;
    public AudioClip GateOpen;
    public AudioClip LevelMusic;
    public AudioClip BossMusic;
    public AudioClip Explosion;
    public AudioClip GameOver;


    public AudioClip[] SurvivalModeMusics;

    HealthDisplay healthDisplay;
    bool doingGameOver;

    public GameObject CanvasObject;

    public static LevelManager CurrentLevel;
    public bool Paused { get; private set; }

    public GameObject GetCanvasObject()
    {
        return CanvasObject;
    }

    public void PlayOneShot(AudioClip clip, bool doRandomPitch)
    {
        efxSource.PlayOneShot(clip);
    }

    public void PlayOneShot(AudioClip[] clips, bool doRandomPitch = true)
    {
        if (clips == null || clips.Length == 0)
            return;
        var clip = clips[Random.Range(0, clips.Length - 1)];
        PlayOneShot(clip, doRandomPitch);
    }
    public void PlayExplosion()
    {
        efxSource.PlayOneShot(Explosion);
    }

    public void PlayGameOver()
    {
        //  efxSource.Stop();
        musicSource.Stop();

        musicSource.PlayOneShot(GameOver);

    }
    public void PlayPlayerTakeDamage()
    {
        efxSource.PlayOneShot(PlayerTakeDamage);
    }

    public void PlayGateOpen()
    {
        efxSource.PlayOneShot(GateOpen);
    }

    void FlipX(GameObject ganeObject)
    {
        SpriteRenderer renderer = gameObject.GetComponent<SpriteRenderer>();
        if (renderer != null)
            renderer.flipX = true;
    }


    public void ShowPlayerHealth(int health)
    {
        var txtHealth = this.txtHealth.GetComponent<Text>();
        if (txtHealth != null)
            txtHealth.text = "Health: " + health;
        if (healthDisplay != null)
            healthDisplay.RedisplayHealth(health);

    }
    void FlipY(GameObject ganeObject)
    {
        SpriteRenderer renderer = gameObject.GetComponent<SpriteRenderer>();
        if (renderer != null)
            renderer.flipY = true;
    }

    [HideInInspector]
    public int score = 0;
    public void UpdateScore(int points)
    {
        score += points;
        var guitext = txtScore.GetComponent<Text>();
        guitext.text = "Score: " + score;
    }

    IEnumerator FadeUp()
    {
        while (musicSource.volume < 1.0f)
        {
            yield return new WaitForSeconds(0.4f);
            musicSource.volume = musicSource.volume + 0.5f;
        }

    }
    void FadeInMusic()
    {
        if (GameManager.GetInstance().GameOptions.Mode == GameMode.Survival)
            musicSource.clip = SurvivalModeMusics[Random.Range(0, SurvivalModeMusics.Length)];
        else
            musicSource.clip = LevelMusic;
        musicSource.loop = true;
        musicSource.volume = 0.0f;
        StartCoroutine("FadeUp");
        //    musicSource.l
        musicSource.Play();
    }

    // Use this for initialization

    void Start()
    {
        Debug.Log("Trying to run Start on Level Manager in main screen");
        LevelManager.CurrentLevel = this;
        SetChanceRanges();
        GameOptions = GameManager.GetInstance().GameOptions; // new GameOptions();


        healthDisplay = FindObjectOfType(typeof(HealthDisplay)) as HealthDisplay;
        ResetLevel();

        this.FrontGateLeft = Instantiate(FrontGatePreFab, FrontGate1.position, Quaternion.identity);
        this.FrontGateRight = Instantiate(FrontGatePreFab, FrontGate2.position, Quaternion.identity);

        this.SideGateTopLeft = Instantiate(SideGateLeftPreFab, LeftGate1.position, Quaternion.identity);
        this.SideGateBottomLeft = Instantiate(SideGateLeftPreFab, LeftGate2.position, Quaternion.identity);

        this.SideGateTopRight = Instantiate(SideGateRightPreFab, RightGate1.position, Quaternion.identity);
        this.SideGateBottomRight = Instantiate(SideGateRightPreFab, RightGate2.position, Quaternion.identity);



        this.TrapTopLeft = Instantiate(TrapDoorPreFab, TrapDoor1.position, Quaternion.identity);
        this.TrapTopRight = Instantiate(TrapDoorPreFab, TrapDoor2.position, Quaternion.identity);

        this.TrapBottomLeft = Instantiate(TrapDoorPreFab, TrapDoor3.position, Quaternion.identity);
        this.TrapBottomRight = Instantiate(TrapDoorPreFab, TrapDoor4.position, Quaternion.identity);

        TrapDoors = new List<global::TrapDoorBehaviour>();
        TrapDoors.Add(this.TrapTopLeft.GetComponent<TrapDoorBehaviour>());
        TrapDoors.Add(this.TrapTopRight.GetComponent<TrapDoorBehaviour>());
        TrapDoors.Add(this.TrapBottomLeft.GetComponent<TrapDoorBehaviour>());
        TrapDoors.Add(this.TrapBottomRight.GetComponent<TrapDoorBehaviour>());


        txtGameOver.SetActive(false);
        txtWaveText.SetActive(false);

        this.BottomGateLeft = Instantiate(BottomGatePreFab, BottomGate1.position, Quaternion.identity);
        this.BottomGateRight = Instantiate(BottomGatePreFab, BottomGate2.position, Quaternion.identity);
        FadeInMusic();
        StartLevelEventCoRoutines();
        Paused = false;
    }

    ChanceSet<GameObject> ChanceRanges;
    ChanceSet<GameObject> PickupChancesRanges;

    void SetChanceRanges()
    {

        ChanceRanges = new ChanceSet<GameObject>();
        ChanceRanges.Build(TierChance, TierEnemyPreFab);

        PickupChancesRanges = new ChanceSet<GameObject>();
        PickupChancesRanges.Build(PickupChance, PickupPrefabs);

    }

    public float LeavePickupChance = 20.0f;

    public void EnemyDie(GameObject enemy)
    {
        if (UnityEngine.Random.Range(0, 100) < LeavePickupChance)
        {
            LeavePickup(enemy.transform.position);
        }
    }

    public int WeaponPickupChance = 30;
    void LeavePickup(Vector3 position)
    {
        int pickUpTypeChance = UnityEngine.Random.Range(0, 100);
        if (pickUpTypeChance < WeaponPickupChance)
        {

            GameObject weapon = Instantiate(WeaponPickUpPreFab, position, Quaternion.identity);
            PlayerWeapon weaponType;
            if (player.CurrentWeapon == PlayerWeapon.SimpleGun)
                weaponType = PlayerWeapon.Sword;
            else
                weaponType = PlayerWeapon.SimpleGun;
            weapon.GetComponent<PickupWeapon>().SetWeapon(weaponType);

        }
        else
        {
            int chance = UnityEngine.Random.Range(0, PickupPrefabs.Length);
            Instantiate(PickupPrefabs[chance], position, Quaternion.identity);
        }
    }
    public void InitLevels()
    {
        CreateLevels();
    }
    public bool IsPLayerAlive()
    {
        if (this.player != null && this.player.gameObject != null)
        {
            return true;
        }
        return false;
    }

    public Vector2 GetPlayerPosition()
    {
        return this.player.gameObject.transform.position;
    }

    public int WaveNumber;
    public float WaveFactorMultiplier = 0.8f;
    public float WaveFactorMinMultiplier = 0.6f;

    float[,] waveNumbers = new float[,] { { 0.6f, 0.8f } };

    class LevelFactor
    {
        public float MinTime;
        public float MaxTime;
        public int MinEnemies;
        public int MaxEnemies;
        public bool Done;
        public bool Tier1;
        public bool Tier2;
        public int MinTrapDoorOpen;
        public int MaxTrapDoorOpen;

        public LevelFactor(float minTime, float maxTime, int minEnemies, int maxEnemies, int minTrapDoorOpen, int maxTrapDoorOpen)
        {
            this.MinTime = minTime;
            this.MaxTime = maxTime;
            this.MinEnemies = minEnemies;
            this.MaxEnemies = maxEnemies;
            this.MinTrapDoorOpen = minTrapDoorOpen;
            this.MaxTrapDoorOpen = maxTrapDoorOpen;
            this.Done = false;
            Tier1 = true;
            Tier2 = true;
        }
    }
    List<LevelFactor> Levels;
    void CreateLevels()
    {
        Levels = new List<LevelFactor>();
        Levels.Add(new LevelFactor(1, 1, 1, 1, 0, 0) { Tier1 = true, Tier2 = false }); //1 
        Levels.Add(new LevelFactor(1, 1, 2, 2, 0, 0) { Tier1 = true, Tier2 = false }); //2
        Levels.Add(new LevelFactor(2, 4, 2, 3, 1, 4)); //3
        Levels.Add(new LevelFactor(4, 6, 2, 4, 1, 4)); //4
        Levels.Add(new LevelFactor(6, 6, 3, 4, 0, 2)); //5
        Levels.Add(new LevelFactor(6, 6, 3, 5, 0, 2)); //6
        Levels.Add(new LevelFactor(6, 6, 4, 5, 0, 2)); //7
        Levels.Add(new LevelFactor(6, 6, 4, 5, 0, 2)); //8 
        Levels.Add(new LevelFactor(6, 6, 4, 6, 0, 2)); //9
        Levels.Add(new LevelFactor(6, 6, 4, 6, 1, 2)); //10
        Levels.Add(new LevelFactor(6, 6, 4, 7, 1, 2)); //11
        Levels.Add(new LevelFactor(6, 6, 4, 5, 1, 2)); //12
        Levels.Add(new LevelFactor(6, 6, 5, 7, 1, 2)); //13
        Levels.Add(new LevelFactor(6, 6, 5, 5, 1, 2)); //14
        Levels.Add(new LevelFactor(6, 9, 6, 8, 1, 2)); //15
        Levels.Add(new LevelFactor(6, 9, 6, 6, 1, 3)); //16
        Levels.Add(new LevelFactor(6, 9, 6, 8, 1, 3)); //17
        Levels.Add(new LevelFactor(6, 9, 6, 7, 1, 4)); //18
        Levels.Add(new LevelFactor(6, 9, 6, 9, 1, 4)); //19
        Levels.Add(new LevelFactor(6, 9, 6, 9, 2, 4)); //20
        Levels.Add(new LevelFactor(6, 6, 5, 6, 2, 4)); //21
        Levels.Add(new LevelFactor(6, 9, 7, 8, 2, 4)); //22
        Levels.Add(new LevelFactor(6, 6, 7, 9, 3, 4)); //23
        Levels.Add(new LevelFactor(6, 9, 7, 9, 3, 4)); //24
        Levels.Add(new LevelFactor(6, 6, 7, 7, 4, 4)); //25
    }

    public float MinTime = 4;
    public float MaxTime = 7;

    public float[] TierChance;
    public GameObject[] TierEnemyPreFab;


    string GetWaveMessage()
    {
        if (WaveNumber == 5)
        {
            return "Wave Number - 5 " + "Not Bad";
        }

        if (WaveNumber == 10)
        {
            return "Wave Number - 10 " + "You just Might make it out alive";
        }

        if (WaveNumber == 15)
        {
            return "Wave Number - 15 " + "How much more ?? ";
        }

        return "Wave Number - " + this.WaveNumber;
    }

    public int GetNumberOfEnemiesForWave()
    {
        var level = GetLevelForWave();
        return (int)(Random.Range(level.MinEnemies, level.MaxEnemies));
    }

    LevelFactor GetLevelForWave()
    {
        LevelFactor level;
        try
        {
            if (WaveNumber < Levels.Count)
                level = Levels[WaveNumber - 1];
            else
                level = Levels[Levels.Count - 1];
        }
        catch (System.Exception ex)
        {
            //Debug.LogFormat("Exception hit in GetLevelForWave {0}", ex.Message);
            return null;
        }
        return level;
    }


    LevelFactor GetSurvivalLevelForWave()
    {
        int minTime = 4;
        int maxTime = 10;
        int minEnemies = 1;
        int maxEnemies = WaveNumber;
        int mintrapdoorNumber = 0;// WaveNumber / 3;

        int maxtrapdoorNumber = 4;
        LevelFactor level = new LevelFactor(minTime, maxTime, minEnemies, maxEnemies, mintrapdoorNumber, maxtrapdoorNumber);

        return level;
    }

    public float GetNumberOfSecondsForNextWave()
    {
        var level = GetLevelForWave();
        return level.MinTime;
    }

    int GetTotalNumberOfEnemies()
    {
        GameObject[] enemies = GameObject.FindGameObjectsWithTag("Enemy");
        int total = enemies.Length;
        return total;
    }

    public int SurvivalWaveFactor = 3;

    public int GetMaxNumberOfEnemies()
    {
        if (WaveNumber > 30)
            return WaveNumber / SurvivalWaveFactor;
        if (WaveNumber > 20)
            return 8;
        if (WaveNumber > 12)
            return 6;
        if (WaveNumber > 6)
            return 5;
        return 3;
    }

    IEnumerator AutoEvent()
    {
        WaveNumber++;
        if (WaveNumber == 1)
        {
            ShowWaveTextMessage(string.Format("Wave {0} - It Begins", WaveNumber));
            yield return new WaitForSeconds(2.0f);
            AddLionEnemy(1);
            TrapTopRight.GetComponent<TrapDoorBehaviour>().Open();
            StartCoroutine("AutoEvent");
            yield break;
        }
        if (WaveNumber == 2)
        {
            yield return new WaitForSeconds(5.0f);
            ShowWaveTextMessage("Too Easy?");
            AddLionEnemy(1);
            AddLionEnemy(2);
            StartCoroutine("AutoEvent");
            yield break;
        }

        var level = GetLevelForWave();
        float nextWaveTime = GetNumberOfSecondsForNextWave();
        yield return new WaitForSeconds(nextWaveTime);

        while (GetTotalNumberOfEnemies() > GetMaxNumberOfEnemies())
        {
            yield return new WaitForSeconds(2);
        }

        ShowWaveTextMessage(GetWaveMessage());

        int numberOfEnemies = GetNumberOfEnemiesForWave();

        AddEnemiesToScene(numberOfEnemies);

        SetTrapDoorsForLevel(level);
        if (WaveNumber < Levels.Count - 1)
            StartCoroutine("AutoEvent");
        else
            StartCoroutine("BossFight");

    }

    IEnumerator AutoEventSurvivalMode()
    {

        WaveNumber++;
        LevelFactor level = GetSurvivalLevelForWave();
        float nextWaveTime = level.MinTime;

        yield return new WaitForSeconds(nextWaveTime);


        while (GetTotalNumberOfEnemies() > GetMaxNumberOfEnemies())
        {
            yield return new WaitForSeconds(2);
        }


        ShowWaveTextMessage("Wave Number - " + this.WaveNumber);

        int numberOfEnemies = 2;
        if (level != null)
            numberOfEnemies = (int)Random.Range(level.MinEnemies, level.MaxEnemies); // GetNumberOfEnemiesForWave();

        AddEnemiesToScene(numberOfEnemies);

        SetTrapDoorsForLevel(level);


        StartCoroutine("AutoEventSurvivalMode");

    }

    IEnumerator EndLevel()
    {
        yield return new WaitForSeconds(3);
        while (GetTotalNumberOfEnemies() > 0)
        {

            yield return new WaitForSeconds(2);

        }
        yield return new WaitForSeconds(3);
        GameManager.GetInstance().UpdateScoreBoard(score);
        SceneManager.LoadScene("WinScreen");

    }

    bool IsBossAlive()
    {
        var boss = FindObjectOfType<EmpBossBehaviour>();
        if (boss != null)
        {
            return !boss.IsDead();
        }
        return false;
    }

    public int BossWaveMin = 1;
    public int BossWaveMax = 3;
    IEnumerator BossWave()
    {
        yield return new WaitForSeconds(4.0f);

        while (GetTotalNumberOfEnemies() > GetMaxNumberOfEnemies())
        {
            yield return new WaitForSeconds(2);
        }

        if (!IsBossAlive())
            yield break;


        int numberOfEnemies = (int)(Random.Range(BossWaveMin, BossWaveMax));

        AddEnemiesToScene(numberOfEnemies);

        StartCoroutine("BossWave");

    }

    IEnumerator BossFight()
    {
        while (GetTotalNumberOfEnemies() > 0)
        {
            yield return new WaitForSeconds(2);
        }

        CloseAllTrapDoors();


        ShowWaveTextMessage("Face The Emperor");
        this.musicSource.Stop();
        this.musicSource.clip = BossMusic;
        this.musicSource.Play();

        yield return new WaitForSeconds(2.0f);
        BossHealthBar.SetActive(true);
        BossHealthBar.GetComponent<HealthBar>().SetValue(1.0f);
        AddEnemy(0, EmperorPreFab);
        StartCoroutine("BossWave");
        StartCoroutine("EndLevel");
    }

    void CloseAllTrapDoors()
    {
        for (int i = 0; i < TrapDoors.Count; i++)
        {
            if (TrapDoors[i].IsOpen)
                TrapDoors[i].StartClose();
        }
    }

    private void SetTrapDoorsForLevel(LevelFactor level)
    {
        int trapNumber = Random.Range(level.MinTrapDoorOpen, level.MaxTrapDoorOpen);
        List<int> traps = GetRandomSetInRange(trapNumber, 0, this.TrapDoors.Count - 1);

        foreach (int i in traps)
            this.TrapDoors[i].Open();

        for (int i = 0; i < TrapDoors.Count; i++)
        {
            if (!traps.Contains(i) && TrapDoors[i].IsOpen)
                TrapDoors[i].StartClose();
        }
    }

    private void AddEnemiesToScene(int numberOfEnemies)
    {
        List<int> doors = new List<int>();
        for (int i = 1; i <= 8; i++)
        {
            doors.Add(i);
        }

        for (int i = 0; i < numberOfEnemies; i++)
        {
            int doorNum;
            doorNum = Random.Range(1, 8);
            doors.Remove(doorNum);
            GameObject enemyPreFab = ChanceRanges.GetRandom();
            if (enemyPreFab != null)
                AddEnemy(doorNum, enemyPreFab);

        }
    }

    List<int> GetRandomSetInRange(int number, int min, int max)
    {
        List<int> set = new List<int>();
        List<int> doors = new List<int>();
        for (int i = min; i <= max; i++)
        {
            doors.Add(i);
        }

        for (int i = 0; i < number; i++)
        {
            int doorNum;
            do
            {
                doorNum = Random.Range(min, max);
            } while (!doors.Contains(doorNum));
            doors.Remove(doorNum);
            set.Add(doorNum);
        }
        return set;
    }
    public float WaveTextDuration = 1.5f;
    IEnumerator HideWaveText()
    {
        yield return new WaitForSeconds(WaveTextDuration);
        txtWaveText.SetActive(false);
    }

    void ShowWaveTextMessage(string msg)
    {
        var waveText = txtWaveText.GetComponent<Text>();
        txtWaveText.SetActive(true);
        waveText.text = msg;
        StartCoroutine("HideWaveText");
    }

    public float PlayerResetTime = 1.0f;
    IEnumerator PlayerReset(GameObject player)
    {
        yield return new WaitForSeconds(PlayerResetTime);


        player.SetActive(true);
        var pl = player.GetComponent<PlayerScript>();
        pl.DoReset();
        pl.RunTemporaryImmunity();

    }

    public void DoPlayerReset(GameObject player)
    {
        player.transform.position = PlayerStart.position;
        player.SetActive(false);
        StartCoroutine("PlayerReset", player);

    }

    public GameObject TridentPreFab;
    public float TridentSpeed = 2.0f;
    void ShootTrident()
    {
        Vector3 postion = this.transform.position + new Vector3(0.5f, 0.5f, 0);

        Quaternion rotation = Quaternion.identity;

        GameObject bullet = Instantiate(TridentPreFab, postion, rotation) as GameObject;
        Rigidbody2D bullet_body = bullet.GetComponent<Rigidbody2D>();
        Vector2 fireDirn = new Vector2(Random.Range(-1.0f, 1.0f), Random.Range(-1.0f, 1.0f));
        fireDirn.Normalize();
        bullet_body.velocity = fireDirn * TridentSpeed;
        float smallAngle = Vector2.Angle(Vector2.up, fireDirn);
        if (fireDirn.x > 0)
            smallAngle = 360 - smallAngle;

        bullet_body.rotation = smallAngle;
    }

    IEnumerator TestSpawnTrident()
    {
        while (true)
        {
            ShootTrident();
            yield return new WaitForSeconds(2.0f);
        }
    }

    IEnumerator TestWave1()
    {
        ShowWaveTextMessage(string.Format("Wave {0} - It Begins", WaveNumber));
        yield return new WaitForSeconds(2.0f);
        AddLionEnemy(1);
        while (true)
        {
            TrapTopRight.GetComponent<TrapDoorBehaviour>().Open();
            yield return new WaitForSeconds(4.0f);
            TrapTopRight.GetComponent<TrapDoorBehaviour>().StartClose();
            yield return new WaitForSeconds(4.0f);
        }
    }

    IEnumerator TestEnemy1()
    {
        ShowWaveTextMessage(string.Format("Wave {0} - It Begins", WaveNumber));
        yield return new WaitForSeconds(2.0f);
        while (true)
        {
            AddEnemy(0, EnemyTridentPreFab);
            TrapTopRight.GetComponent<TrapDoorBehaviour>().Open();
            yield return new WaitForSeconds(6.0f);
            TrapTopRight.GetComponent<TrapDoorBehaviour>().StartClose();
            yield return new WaitForSeconds(6.0f);
          
        }
    }

    public bool LevelOn = true;


    public void StartLevelEventCoRoutines()
    {
        //  StartCoroutine("TestEnemy1");
        //   return;
        if (LevelOn)
        {
            //   StartCoroutine("BossFight");
            //StartCoroutine("TestEnemy1");
            //   StartCoroutine("TestSpawnTrident");
            //      StartCoroutine("TestWave1");

            if (GameManager.GetInstance().GameOptions.Mode == GameMode.Survival)
                StartCoroutine("AutoEventSurvivalMode");
            else
                StartCoroutine("AutoEvent");
        }

    }
    
    GameObject GetGateForNo(int index)
    {
        switch (index)
        {
            case 1: return this.FrontGateLeft;
            case 2: return this.FrontGateRight;
            case 3: return this.SideGateTopRight;
            case 4: return this.SideGateBottomRight;
            case 5: return this.BottomGateRight;
            case 6: return this.BottomGateLeft;
            case 7: return this.SideGateBottomLeft;
            case 8: return this.SideGateTopLeft;
            default: return this.FrontGateLeft;
        }

    }
    public void AddLionEnemy(int gateNo)
    {
        AddEnemy(gateNo, this.BlueLionPreFab);
    }

    public void AddTridentEnemy(int gateNo)
    {
        AddEnemy(gateNo, this.EnemyTridentPreFab);
    }


    public void AddEnemy(int gateNo, GameObject enemyPreFab)
    {
        GateEnter enterAction = null;
        GameObject gate = GetGateForNo(gateNo);

        enterAction = gate.GetComponent<GateEnter>();
        if (enterAction != null)
        {
            PlayGateOpen();
            enterAction.OpenAndEnterObject(enemyPreFab);
        }
    }

    public void ExitToMainScreen()
    {
        SceneManager.LoadScene("TitleScreen", LoadSceneMode.Single);
    }

    public int GameOverTimer = 5;
    IEnumerator ReturnToTitle()
    {
        yield return new WaitForSeconds(GameOverTimer);
        ExitToMainScreen();
    }

    public GameObject PausePanel;
    void CheckGlobalInput()
    {
        if (Input.GetKeyDown(KeyCode.Escape) || Input.GetButtonDown("StartButton"))
        {
            Pause();
        }

        if (Input.GetKeyDown(KeyCode.D))
            Camera.main.GetComponent<ScreenShaker>().Run();
    }

    public GameObject MouseCursor;
    public void Pause()
    {
        Paused = true;
        Time.timeScale = 0;
        PausePanel.SetActive(true);
        PausePanel.GetComponent<PauseMenuBehaviour>().Initialise();
        if (MouseCursor != null)
            MouseCursor.SetActive(false);
    }
    public void UnPause()
    {
        Time.timeScale = 1.0f;
        Paused = false;
        PausePanel.SetActive(false);
        if (GameOptions.ControlScheme == InputScheme.Mouse && MouseCursor != null)
            MouseCursor.SetActive(true);
    }

    public float GameOverButtonDelay = 1.0f;
    IEnumerator TurnOnGameOverResetButtons()
    {
        yield return new WaitForSeconds(GameOverButtonDelay);
    //    GameOverPanelButtons.SetActive(true);
        GameOverPanelButtonRestart.SetActive(true);
        GameOverPanelButtonExit.SetActive(true);
    }
    void Update()
    {
        CheckGlobalInput();
        if (player == null)
            return;

        if (player.IsDead())
        {
            if (doingGameOver)
                return;
            doingGameOver = true;
            GameOverPanel.SetActive(true);
            //GameOverPanelButtons.SetActive(false);
            GameOverPanelButtonExit.SetActive(false);
            GameOverPanelButtonRestart.SetActive(false);
            GameOverPanel.GetComponent<GameOverPanelBehaviour>().Redisplay();
            GameManager.GetInstance().UpdateScoreBoard(score);
            PlayGameOver();
            StartCoroutine("TurnOnGameOverResetButtons");

        }
    }

    public void RestartGame()
    {
        SceneManager.LoadScene("MainScene");
    }

    public void ResetLevel()
    {
        UnPause();
        BossHealthBar.SetActive(false);
        this.doingGameOver = false;
        this.score = 0;
        InitLevels();
        this.WaveNumber = StartLevel;
        GameObject playerObj = Instantiate(PlayerPreFab, PlayerStart.position, Quaternion.identity);
        this.player = playerObj.GetComponent<PlayerScript>() as PlayerScript;
        GameOverPanel.SetActive(false);
        IntroPanel.SetActive(true);
        PausePanel.SetActive(false);
        IntroPanel.GetComponent<IntroPanelBehaviour>().PlayIntro();
        UpdateScore(0);
    }
}
