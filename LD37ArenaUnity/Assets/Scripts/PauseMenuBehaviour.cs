﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PauseMenuBehaviour : MonoBehaviour {

    public GameObject pnlControls;
    public GameObject pnlMain;
    LevelManager levelManager;
    GameOptions gameOptions;
    Button[] mainScreenButtons;
    Button[] ctrlScreenButtons;
    // Use this for initialization
    void Start () {
        levelManager = FindObjectOfType(typeof(LevelManager)) as LevelManager;
        gameOptions = GameManager.GetInstance().GameOptions;
        mainScreenButtons = pnlMain.GetComponentsInChildren<Button>();
        ctrlScreenButtons = pnlControls.GetComponentsInChildren<Button>();
    }
	

    public void Initialise()
    {
        GoBackToMainMenu();
    }


    public void btnExitClick()
    {

    }

    public void btnResumeClick()
    {
        levelManager.UnPause();
       
    }

    void Update()
    {

    }

    bool IsFireButtonDown()
    {
        return Input.GetKeyDown(KeyCode.Space) || Input.GetButtonDown("Fire1");
    }

    public void btnControlsClick()
    {
        pnlMain.SetActive(false);
        pnlControls.SetActive(true);
        pnlControls.GetComponent<PanelControlsOptionBehaviour>().SetCurrentlySelected(GameManager.GetInstance().GameOptions.ControlScheme);
    }

    public void btnControlClassicClick()
    {
        GameManager.GetInstance().GameOptions.ControlScheme = InputScheme.Classic;
        GoBackToMainMenu();
    }

    public void btnControlTwinStickClick()
    {
        GameManager.GetInstance().GameOptions.ControlScheme = InputScheme.Twinstick;
        GoBackToMainMenu();
    }

    public void btnControlMouseClick()
    {
        GameManager.GetInstance().GameOptions.ControlScheme = InputScheme.Mouse;
        GoBackToMainMenu();
    }

    void GoBackToMainMenu()
    {
        pnlControls.SetActive(false);
        pnlMain.SetActive(true);
        if (mainScreenButtons != null && mainScreenButtons.Length > 0)
            mainScreenButtons[0].Select();
    }

}
