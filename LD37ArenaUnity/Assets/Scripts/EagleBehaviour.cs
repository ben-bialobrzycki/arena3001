﻿using Assets.Scripts;
using System.Collections;
using UnityEngine;


public class EagleBehaviour : MonoBehaviour {


    //float number of enums must match Chances
    public enum EagleState { FlyCircle,FlyStraight, Attack };
    ChanceSet<EagleState> stateChanceSet;

    public float CircleSpeed = 2.0f;
    public float AttackSpeed = 3.0f;
    LevelManager levelManager;
    Rigidbody2D body;
    EagleState CurrentState;
  
    Vector2 CurrentDirection;
    Vector2 TargetPosition;
  

    public float ChanceFlyCircle = 2.0f;
    public float ChanceFlyStraight = 2.0f;
    public float ChanceAttack = 2.0f;

    void Start()
    {
        levelManager = FindObjectOfType(typeof(LevelManager)) as LevelManager;
        this.body = GetComponent<Rigidbody2D>();
        SetDirection();

        stateChanceSet = new ChanceSet<EagleState>();
       
        stateChanceSet.Build(
            new float[]      { ChanceFlyCircle, ChanceFlyStraight, ChanceAttack }, 
            new EagleState[] { EagleState.FlyCircle,EagleState.FlyStraight, EagleState.Attack});
        StartCoroutine("SwitchState");
    }

    public float TurnFactor = 3f;
    public float MinStateChange = 1.0f;
    public float MaxStateChange = 6.0f;

    IEnumerator SwitchState()
    {
        float time = UnityEngine.Random.Range(MinStateChange, MaxStateChange);
        yield return new WaitForSeconds(time);
        EagleState nextState = stateChanceSet.GetRandom();
        StartNewState(nextState);
       
    }

    public float PlayerMinDistance = 1.0f;
    public float PlayerMaxDistance = 3.0f;

    public float CircleMaxFactor = 2.0f;
    public float CircleMinFactor = 0.5f;

    void StartNewState(EagleState newState)
    {
        if (newState == EagleState.FlyStraight)
        {
            Vector2 player = levelManager.GetPlayerPosition();
            TargetPosition = player + (Random.Range(PlayerMinDistance,PlayerMaxDistance) * MovementUtil.RandomUnitVector());
            CurrentDirection = TargetPosition - body.position;
        }
        if (newState == EagleState.Attack)
        {
            TargetPosition = levelManager.GetPlayerPosition();
            CurrentDirection = TargetPosition - body.position;
        }
        if (newState == EagleState.FlyCircle)
        {
            TurnFactor = Random.Range(CircleMinFactor, CircleMaxFactor);
        }
        CurrentState = newState;
        if (CurrentState != EagleState.Attack) //finish attack only when hit target
        {
            StopAllCoroutines();
            StartCoroutine("SwitchState");
        }
    }

    void SetDirection()
    {
        Vector2 dirn = Vector2.down; 
        dirn.Normalize();
        SetMovementForDirection(dirn);
    }
    // Update is called once per frame
    void Update()
    {
        Vector2 newDirection = CurrentDirection;
        Vector2 playerPos = this.levelManager.GetPlayerPosition();
        
        if (CurrentState == EagleState.FlyCircle)
        {
            Vector2 perpDirection = new Vector2(CurrentDirection.y, -CurrentDirection.x);
            newDirection = CurrentDirection + (TurnFactor * Time.deltaTime * perpDirection);
        }
        if (CurrentState == EagleState.FlyStraight)
        {
            newDirection = CurrentDirection;
        }
        if (CurrentState == EagleState.Attack  || CurrentState == EagleState.FlyStraight)
        {
            if (this.body.OverlapPoint(TargetPosition))
            {
                StartNewState(EagleState.FlyCircle);
                return;
            }
            newDirection = CurrentDirection;
        }

        newDirection.Normalize();
        SetMovementForDirection(newDirection);

    }

    Vector2 velocity;
    void SetMovementForDirection(Vector2 dirn)
    {
        CurrentDirection = dirn;
        float speed;
        if (CurrentState == EagleState.Attack)
            speed = AttackSpeed;
        else
            speed = CircleSpeed;
      //  this.body.velocity = dirn * speed;
        velocity = dirn * speed;
        float smallAngle = MovementUtil.GetRotationAngleForDirection(dirn);
        body.rotation = smallAngle;
    }

    private void FixedUpdate()
    {
        this.body.MovePosition(this.body.position + (velocity * Time.deltaTime));
    }
}
