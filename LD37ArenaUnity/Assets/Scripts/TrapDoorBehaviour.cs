﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrapDoorBehaviour : MonoBehaviour {

    public float CloseTime = 0.5f;
    public float OpenTime = 0.5f;
    public float AnimatorSpeed = 0.7f;
    SpriteRenderer renderer;

    BoxCollider2D boxCollider;

    [HideInInspector]
    public bool IsOpen;
    Animator animator;
    // Use this for initialization

    bool isClosing;
    IEnumerator CloseGate()
    {
        this.animator.speed = AnimatorSpeed;
        this.animator.Play("FrontGateClose");
        yield return new WaitForSeconds(CloseTime);
    }

    public void CloseGateEvent()
    {
        IsOpen = false;
        boxCollider.enabled = false;
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        CheckCollisions(collision);
    }

    private void CheckCollisions(Collision2D collision)
    {
        if (collision.gameObject.tag == TagUtil.Enemy)
        {
            var fallable = collision.gameObject.GetComponent<FallBehaviour>();
            if (fallable == null)
                return;
            var knockBack = collision.gameObject.GetComponent<KnockBackBehaviour>();
            if (knockBack != null && knockBack.OnKnockBack)
            {
                fallable.TakeFall(this.gameObject);
            }

        }
        if (collision.gameObject.tag == "Player")
        {
            var player = collision.gameObject.GetComponent<PlayerScript>();
            if (player != null)
            {
                player.TakeFall(this.gameObject);
            }
        }
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        CheckCollisions(collision);
    }

    public void StartClose()
    {
         StartCoroutine("CloseGate");
    }

    void OpenGate()
    {

    }


    IEnumerator FinishOpen()
    {

        yield return new WaitForSeconds(OpenTime);
    }


    public void FinishOpenEvent()
    {
        Debug.Log("Trapdoor Finish Open Event Called");
        IsOpen = true;
        boxCollider.enabled = true;
    }
    public void Open()
    {
        this.animator.speed = AnimatorSpeed;
        this.animator.Play("FrontGateOpen");

        if (!isClosing)
        {
            StartCoroutine("FinishOpen");
        }
        
    }

    void Start()
    {
        this.boxCollider = GetComponent<BoxCollider2D>();
        this.animator = GetComponent<Animator>();
        this.renderer = GetComponent<SpriteRenderer>();
        this.animator.speed = 0;
        this.boxCollider.enabled = IsOpen;
    }
    // Update is called once per frame
    void Update () {

      //  var state = this.animator.GetCurrentAnimatorStateInfo(0);

        //if (state.IsName("FrontGateOpen") && state.normalizedTime >= 0.7f && !IsOpen)
        //    FinishOpenEvent();
        //if (state.IsName("FrontGateClose") && state.normalizedTime >= 0.7f && IsOpen)
        //    CloseGateEvent();
        //if (IsOpen)
        //    renderer.color = Color.red;
        //else
        //    renderer.color = Color.white;

    }
}
