﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerFall: MonoBehaviour {

    public float LifeTime = 1.0f;
    public AudioClip Sound;
    LevelManager levelManager;

    IEnumerator FadeOut()
    {

        yield return new WaitForSeconds(LifeTime);
        this.gameObject.SetActive(false);
        Destroy(this.gameObject);

    }

    // Use this for initialization
    void Start () {
        levelManager = FindObjectOfType(typeof(LevelManager)) as LevelManager;
        levelManager.PlayOneShot(this.Sound,false);
        StartCoroutine(FadeOut());

    }
	
	// Update is called once per frame
	void Update () {
		
	}
}
