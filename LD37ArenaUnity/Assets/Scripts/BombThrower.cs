﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//For Testing Bomb throwing 
public class BombThrower : MonoBehaviour {
    public GameObject BombPreFab;
	// Use this for initialization
	void Start () {
        StartCoroutine("ThrowBombs");
    }
	
	// Update is called once per frame
	void Update () {
     
	}

    IEnumerator ThrowBombs()
    {
        while (true)
        {
            yield return new WaitForSeconds(2.0f);
            ThrowABomb();
        }
    }

    void ThrowABomb()
    {
        float x = Random.Range(-5, 5);
        float y = Random.Range(-5, 5);
        GameObject bomb = Instantiate(BombPreFab, this.transform.position, Quaternion.identity);
        bomb.GetComponent<BombBehaviour>().ThrowToTarget(this.transform.position, new Vector2(x, y));
    }
}
