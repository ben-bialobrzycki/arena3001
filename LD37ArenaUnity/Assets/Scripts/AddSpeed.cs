﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AddSpeed : MonoBehaviour {

    public float SpeedAmount = 2.0f;
    public float SpeedDuration = 20.0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.tag == "Player")
        {
            var player = collision.gameObject.GetComponent<PlayerScript>();
            if (player != null)
            {
                player.AddSpeed(this.SpeedAmount,SpeedDuration);
            }
            Destroy(this.gameObject);
        }

      
    }
}
