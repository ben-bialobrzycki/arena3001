﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InputScheme {Classic, Mouse, Twinstick }
public enum GameMode {Normal, Survival }
public class GameOptions  {

    public InputScheme ControlScheme = InputScheme.Classic; //InputScheme.Twinstick;
    public GameMode Mode = GameMode.Normal;
	// Use this for initialization

}
