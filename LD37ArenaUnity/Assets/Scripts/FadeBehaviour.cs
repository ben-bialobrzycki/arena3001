﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeBehaviour : MonoBehaviour {

    public float TimeToDissapear = 4.0f;

    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    IEnumerator StartFade()
    {
        yield return new WaitForSeconds(TimeToDissapear / 2.0f);
        this.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.7f);

        yield return new WaitForSeconds(TimeToDissapear / 2.0f);
        Destroy(this.gameObject);
    }

    public void RunFade()
    {
        StartCoroutine("StartFade");
    }
}
