﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeflectBehaviour : MonoBehaviour {

    Rigidbody2D body;
	// Use this for initialization
	void Start () {
        body = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void TakeDeflection(Vector3 fromPosition, float force)
    {
       
        if (!this.gameObject.activeSelf)
            return;
        Vector2 dir = this.body.transform.position - fromPosition;
        dir.Normalize();
        body.AddForce(dir * force, ForceMode2D.Force);
        var trident = GetComponent<TridentBehaviour>();
        if (trident != null)
            trident.DeflectDiscard();
    }
}
