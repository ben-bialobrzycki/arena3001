﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class CollisionHelper
{

    public static void TriggerPlayerDamage(GameObject fromObject, Collider2D collision)
    {
        string thisTag = fromObject.tag;
        string otherTag = collision.gameObject.tag;

        if (otherTag == "Player" && thisTag == "EnemyProjectile")
        {
            var player = collision.gameObject.GetComponent<PlayerScript>();
            if (player != null)
            {
                player.TryTakeDamage(fromObject);
            }
        }
    }
}

