﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour {

    public float DamageValue = 1.0f;
	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}


    public bool IsDestroyedByPlayerProjectile;

    public void OnTriggerEnter2D(Collider2D collision)
    {
        string thisTag = this.gameObject.tag;
        string otherTag = collision.gameObject.tag;
        if (otherTag == TagUtil.Player && thisTag == TagUtil.PlayerProjectile)
            return;
        if (otherTag == TagUtil.EnemyProjectile && thisTag == TagUtil.EnemyProjectile)
            return;
        if (otherTag == TagUtil.Enemy && thisTag == TagUtil.PlayerProjectile)
        {
            var enemy = collision.gameObject.GetComponent<Character>();
            if (enemy != null)
            {
                enemy.TakeDamage(this.DamageValue,true);
            }
            var knockback = collision.gameObject.GetComponent<KnockBackBehaviour>();
            if (knockback != null)
            {
                
                knockback.TakeKnockBack(this.gameObject.transform.position,-1);
            }
        }

     
        if (otherTag == "Player" && thisTag == "EnemyProjectile")
        {
            var player = collision.gameObject.GetComponent<PlayerScript>();
            if (player != null)
            {
                player.TryTakeDamage(this.gameObject);
            }
        }

        if (thisTag == "EnemyProjectile" && otherTag == "PlayerProjectile" && !IsDestroyedByPlayerProjectile)
            return;
   
        Destroy(this.gameObject);
    }

    private void OnCollisionStay2D(Collision2D collision)
    {
        if (this.gameObject.tag == TagUtil.PlayerProjectile && collision.gameObject.tag == "Wall")
            Destroy(this.gameObject);
    }


    public void OnCollisionEnter2D(Collision2D collision)
    {
        string thisTag = this.gameObject.tag;
        string otherTag = collision.gameObject.tag;
        if (otherTag == "Player" && thisTag == "PlayerProjectile")
            return;
        if (otherTag == "EnemyProjectile" && thisTag == "EnemyProjectile")
            return;
        if (otherTag == "Enemy" && thisTag == "PlayerProjectile")
        {
            var enemy = collision.gameObject.GetComponent<Character>();
            if (enemy != null)
            {
                enemy.TakeDamage(this.DamageValue, true);
            }
      
        }


        if (otherTag == "Player" && thisTag == "EnemyProjectile")
        {
            var player = collision.gameObject.GetComponent<PlayerScript>();
            if (player != null)
            {
                player.TryTakeDamage(this.gameObject);
            }
        }

        if (thisTag == "EnemyProjectile" && otherTag == "PlayerProjectile" && !IsDestroyedByPlayerProjectile)
            return;
     
        Destroy(this.gameObject);
    }

}
