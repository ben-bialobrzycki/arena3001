﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallBehaviour : MonoBehaviour {

    Animator animator;
    [HideInInspector]
    public bool IsFalling;
  
    public string FallAnimationName = "";
	// Use this for initialization
	void Start () {
        IsFalling = false;
        animator = GetComponent<Animator>();
	}
	
	// Update is called once per frame
	void Update () {
		
	}

    public void TakeFall(GameObject gameObject)
    {
        IsFalling = true;
        animator.Play(FallAnimationName, 0, 0);
       // FallFinished();
    }

    public void FallFinished()
    {
        if (this.gameObject.CompareTag(TagUtil.Enemy))
            GameObject.Destroy(this.gameObject);
        //this.gameObject.SetActive(false);
    }
}
