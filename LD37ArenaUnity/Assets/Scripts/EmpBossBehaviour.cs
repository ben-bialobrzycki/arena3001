﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EmpBossBehaviour : MonoBehaviour, IDamageSubscriber
{
    float WalkSpeed = 1.5f;
    LevelManager levelManager;
    Rigidbody2D body;
    Animator animator;
    public float DamageValue = 5.0f;
    Action CurrentAction;
    public GameObject BulletPreFab;
    public float BulletSpeed = 3.0f;
    bool JustShot = false;
    enum Action { None, Taunt, Attack, Walk };
    Character character;
    HealthBar healthbar;
    Vector2 TargetWalk;
    public AudioClip[] FireClip;
    SpriteRenderer spriteRenderer;
    public float DamageFlashTime = 0.3f;
    public Color DamageColor;
    // Use this for initialization
    void Start()
    {
        levelManager = FindObjectOfType(typeof(LevelManager)) as LevelManager;
        healthbar = FindObjectOfType(typeof(HealthBar)) as HealthBar;
        this.animator = GetComponent<Animator>();
        this.body = GetComponent<Rigidbody2D>();
        this.character = GetComponent<Character>();
        this.character.DamageSubscriber = this;
        this.spriteRenderer = GetComponent<SpriteRenderer>();
        StartWalk();
    }

    Action GetRandomAction()
    {
        float rand = UnityEngine.Random.Range(0, 100);
        if (0 < rand && rand < 20)
        {
            return Action.Taunt;
        }
        if (20 < rand && rand < 60)
        {
            return Action.Walk;
        }
        if (60 < rand && rand < 100 && !JustShot)
        {
            return Action.Attack;
        }
        JustShot = false;
        return Action.Walk;
    }

    IEnumerator SwitchAction(float time)
    {
        yield return new WaitForSeconds(time);
        this.CurrentAction = Action.None; //when no action it will be set to a random one
    }

    void StartTaunt()
    {
        this.body.velocity = Vector2.zero;
        this.animator.SetBool("IsWalking", false);
        this.animator.SetTrigger("DoTaunt");
        StartCoroutine("SwitchAction", 1);
    }
    // Update is called once per frame
    public float AttackLaunchTime = 0.5f;
    IEnumerator FinishAttack()
    {
        yield return new WaitForSeconds(AttackLaunchTime);
        DoFire();
        JustShot = true;
        StartCoroutine("SwitchAction", 2.0f);
    }
    void StartAttack()
    {
        this.animator.SetBool("IsWalking", false);
        this.body.velocity = Vector2.zero;
        this.animator.SetTrigger("DoAction");
        StartCoroutine("FinishAttack");
    }

    public bool IsDead()
    {
        return this.character.IsDead();
    }
    void DoFire()
    {

        if (!levelManager.IsPLayerAlive())
            return;
        levelManager.PlayOneShot(FireClip);
        Vector3 postion = this.transform.position + new Vector3(0.5f, 0.5f, 0);

        Quaternion rotation = Quaternion.identity;
       Vector2 start = Vector2.up;
        int number;
        if (UnityEngine.Random.Range(0, 1) < 0.5f || this.character.Health < this.character.MaximumHealth / 2.0f)
            number = 16;
        else
            number = 8;
      
        for (int i=0;i < number;i++)
        {
            GameObject bullet = Instantiate(BulletPreFab, postion, rotation) as GameObject;
            Rigidbody2D bullet_body = bullet.GetComponent<Rigidbody2D>();
            start  = Quaternion.Euler(0, 0, 360.0f / number) * start;
            Vector2 fireDirn = start;
            fireDirn.Normalize();
            bullet_body.velocity = fireDirn * BulletSpeed; //TODO should be applying force for dynamic bodies rather than setting velocity direct
        }
    }

    //public void OnCollisionStay2D(Collision2D collision)
    //{
    //    //Debug.Log("Hit OnCollisionEnter2D");
    //    if (collision.gameObject.tag == "Player")
    //        return;
    //    if (collision.gameObject.tag == "Enemy")
    //    {
    //        var enemy = collision.gameObject.GetComponent<Enemy>();
    //        if (enemy != null)
    //        {

    //            ;//   this.characterBehaviour.TakeDamage(enemy.DamageValue);
    //        }
    //    }

    //}

    void SetNewAction()
    {
        CurrentAction = GetRandomAction();
        if (CurrentAction == Action.Walk)
        {
            StartWalk();
        }
        if (CurrentAction == Action.Taunt)
        {
            StartTaunt();
        }
        if (CurrentAction == Action.Attack)
        {
            StartAttack();
        }
    }

    private void OnDestroy()
    {
        this.healthbar.gameObject.SetActive(false);
    }

    void Update()
    {
        if (CurrentAction == Action.None)
        {
            SetNewAction();
        }
        if (CurrentAction == Action.Walk)
        {
            DoWalk();
        }

        if (!this.levelManager.IsPLayerAlive())
        {
            this.body.velocity = Vector2.zero;
            return;
        }

    }

    
    void StartWalk()
    {
        this.animator.SetBool("IsWalking", true);
        Vector2 playerPos;
        if (this.levelManager.IsPLayerAlive())
        {
            playerPos = this.levelManager.GetPlayerPosition();
        }
        else
            playerPos = this.levelManager.PlayerStart.position;
        this.TargetWalk = playerPos;
        StartCoroutine("SwitchAction", 5.0f);
    }
    void DoWalk()
    {
        Vector2 dirn = TargetWalk - this.body.position;
        dirn.Normalize();
        this.body.velocity = dirn * WalkSpeed;
    }


    IEnumerator ResetAfterDamage()
    {
        yield return new WaitForSeconds(DamageFlashTime);
        this.spriteRenderer.color = Color.white;
    }

    public void TakeDamage(float damage)
    {
        var character = this.GetComponent<Character>();
        float fullHealth = character.MaximumHealth;
        float perc = character.Health / fullHealth;
        this.healthbar.SetValue(perc);
        this.spriteRenderer.color = DamageColor;
        StartCoroutine("ResetAfterDamage");
    }
}
